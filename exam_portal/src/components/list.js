import { Link } from 'react-router-dom'
const List = (props) => {
  const { title, to } = props
  return (
    <li className='nav-item'>
      <Link
        className='nav-link active fs-5 fw-bold hoverEffect px-3 text-white'
        to={to}>
        {title}
      </Link>
    </li>
  )
}

export default List
