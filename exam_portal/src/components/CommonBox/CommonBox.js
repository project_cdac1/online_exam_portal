import { useEffect, useRef, useState } from 'react'
import Sidebar from '../../components/sidebar/sidebar'
import { ClientData } from '../../Data/sidebarData'

const CommonBox = (props) => {
  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
  })

  return (
    <div ref={wrapperRef} className='vh-100'>
      <div ref={navRef}>
        <Sidebar data={ClientData} title='Institute Name' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-10 boxShadow rounded-3 px-5 py-3 table-responsive text-center'>
          {props.comp}
        </div>
      </div>
    </div>
  )
}

export default CommonBox
