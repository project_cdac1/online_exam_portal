import { Link } from 'react-router-dom'

const SidebarList = (props) => {
  const title = props.title
  const icon = props.icon + ' nav-links-i ms-3 pe-3'
  const to = props.to ? props.to : '#' ;
  return (
    <div className='py-2'>
      <Link to={to} className='list-group-item rounded-3'>
        <i class={icon}></i>
        <span>{title}</span>
      </Link>
    </div>
  )
}

export default SidebarList
