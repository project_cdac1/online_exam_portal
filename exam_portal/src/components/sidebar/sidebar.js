import { Link, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import SidebarList from './sidebarList'

const Sidebar = (props) => {
  const list = props.data.list
  const title = props.title
  const navigate = useNavigate()
  const logOut = () => {
    sessionStorage.clear()
    toast.info('Logged out successfully!')
    navigate('/')
  }
  return (
    <div className='stickyNav'>
      <nav class='navbar trans '>
        <div class='container-fluid row'>
          <div className='col col-3 ps-0 ps-lg-3'>
            <button
              class='navbar-toggler shadow-none noBorder'
              type='button'
              data-bs-toggle='offcanvas'
              data-bs-target='#offcanvasExample'
              aria-controls='offcanvasExample'
              aria-expanded='false'>
              <span class='navbar-toggler-icon toggleButtonStyle'></span>
            </button>
          </div>
          <div className='col col-6'>
            <h2 className='sidebarTitle'>{title}</h2>
          </div>
          <div className='col col-3 pe-0 pe-lg-1'>
            <button className='btn btn-danger logOut'>
              <i class='fa-solid fa-right-from-bracket d-block d-sm-none'></i>
              <span className='d-none d-sm-block' onClick={logOut}>
                <i class='fa-solid fa-right-from-bracket '></i> Log Out
              </span>
            </button>
          </div>
        </div>
      </nav>
      <div>
        <div
          class='offcanvas offcanvas-start sidebar p-3 p-lg-2'
          tabindex='-1'
          id='offcanvasExample'
          aria-labelledby='offcanvasExampleLabel'>
          <div class='offcanvas-header'>
            <Link to='#' className='logo'>
              <img src='../images/logo.png' alt='logo' className='logo'></img>
            </Link>
            <button
              type='button'
              class='btn-close toggleButtonStyle shadow-none'
              data-bs-dismiss='offcanvas'
              aria-label='Close'></button>
          </div>
          <div className='offcanvas-body ps-4'>
            <hr className='horizontalRule'></hr>
            <ul className='list-group ps-2' style={{ margin: 'auto' }}>
              {list.map((l) => {
                l.isHR = l.isHR ? l.isHR : false
                if (l.isHR) {
                  return <hr />
                } else {
                  return <SidebarList icon={l.icon} title={l.title} to={l.to} />
                }
              })}
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Sidebar
