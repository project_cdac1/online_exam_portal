const SidebarDropdown = (props) => {
  return (
    <div class='dropdown-center'>
      <button
        class='btn dropdown-toggle dropbtn ms-1'
        type='button'
        id='dropdownMenuButton'
        data-bs-toggle='dropdown'>
        <i className='fa-solid fa-house ms-3 pe-3'></i>
        <span>Dropdown</span>
      </button>
      <ul class='dropdown-menu dropMenu' aria-labelledby='dropdownMenuButton'>
        <li>
          <a class='dropdown-item' href='#'>
            Action
          </a>
        </li>
        <li>
          <a class='dropdown-item' href='#'>
            Another action
          </a>
        </li>
        <li>
          <a class='dropdown-item' href='#'>
            Something else here
          </a>
        </li>
      </ul>
    </div>
  )
}

export default SidebarDropdown
