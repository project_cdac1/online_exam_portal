const ContactUsBox = () => {
  return (
    <div className='d-grid col-10 col-sm-10 col-lg-7 mx-auto'>
      <div
        className='row border-warning rounded-3 p-lg-5 boxShadow translogin'
        style={{
           marginBottom: '10%',
        }}>
        <div className='col col-12 '>
          <h2 className='title py-2' style={{ color: '#f7b011' }}>
            Contact Us
          </h2>
        </div>
        <div className='col col-12 col-lg-8 my-3'>
          <div className='row my-2 p-2'>
            <div className='col col-12 col-xl-3'>
              <span className='content '> Email :</span>
            </div>
            <div className='col col-12 col-xl-9 '>
              <a
                className='fs-4 fs-bold fst-italic '
                href='mailto:arjunaexamportal@gmail.com'
                style={{ textDecoration: 'none' }}>
                arjunaexamportal@gmail.com
              </a>
            </div>
          </div>

          <div className='row'>
            <div className='col col-12 col-xl-3'>
              <span className='content'> Phone : </span>
            </div>
            <div className='col col-12 col-xl-9'>
              <a
                className='fs-4 fs-bold fst-italic'
                href='tel:7517527737'
                style={{ textDecoration: 'none' }}>
                +91 7517527737
              </a>
            </div>
          </div>
          <div className='row'>
            <div className='col col-12 col-xl-3'></div>
            <div className='col col-12 col-xl-9'>
              <a
                className='fs-4 fs-bold fst-italic '
                href='tel:8329627401'
                style={{ textDecoration: 'none' }}>
                +91 8329627401
              </a>
            </div>
          </div>
          <div className='row'>
            <div className='col col-12 col-xl-3'></div>
            <div className='col col-12 col-xl-9'>
              <a
                className='fs-4 fs-bold fst-italic'
                href='tel:9975207603'
                style={{ textDecoration: 'none' }}>
                +91 9975207603
              </a>
            </div>
          </div>
        </div>
        <div className='col col-12 col-xl-3 d-none d-xl-block'>
          <img
            src='/images/contact-us.png'
            alt='Home '
            width='100%'
            height='100%'
          />
        </div>
      </div>
    </div>
  )
}

export default ContactUsBox
