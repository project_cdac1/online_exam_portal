import { Link } from 'react-router-dom'

const HomeContent = () => {
  return (
    <div className='d-grid col-10 mx-auto my-4 '>
      <div
        className='row py-3 boxShadow rounded-3 p-xl-4 p-0 translogin'
        >

        <div className='col-12 '>
          <h2 className='title' style={{ color: '#f7b011' }}>
            Conducting exams is easy-breezy now
          </h2>
        </div>
        <div className='col col-12 col-lg-6'>
          <p
            className='content'
            style={{
              textAlign: 'justify',
            }}>
            We are one stop solution in the domain of online exams , we provide
            100% secure and transparent platform. Any Organization, Institution
            or University can register here to conduct exams for there students
            or employee.
          </p>
          <div>
            <p
              style={{
                marginTop: '20px',
                color: '#f7b011',
                textAlign: 'center',
                fontWeight: 'bolder',
                fontSize: '300%',
                marginBottom: '25px',
                fontFamily: 'cursive',
              }}>
              Register Here
            </p>
            <Link
              className='btn btn-success btn-lg d-grid gap-2 col-10 mx-auto '
              to={'client/register'}>
              Register
            </Link>
          </div>
        </div>
        <div className='col col-xm-0 col-lg-6 homeImg'>
          <img
            src='./images/homeImage.png'
            alt='Home'
            width='95%'
            height='95%'
          />
        </div>
      </div>
    </div>
  )
}

export default HomeContent
