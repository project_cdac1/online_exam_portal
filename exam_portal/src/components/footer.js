import React from 'react'
import { Link } from 'react-router-dom'

const Footer = () => {
  return (
    <div>
      <footer class=' text-center text-white'>
        <div class='container p-4 pb-0'>
          <section class='mb-1'>
            <a
              class='btn btn-primary btn-floating m-1'
              //   style='background-color: #3b5998;'
              href='#!'
              role='button'
              style={{ backgroundColor: '#3b5998' }}>
              <i class='fab fa-facebook-f'></i>
            </a>

            <a
              class='btn btn-primary btn-floating m-1'
              //   style='background-color: #55acee;'
              href='#!'
              role='button'
              style={{ backgroundColor: '#55acee' }}>
              <i class='fab fa-twitter'></i>
            </a>

            <a
              class='btn btn-primary btn-floating m-1'
              //   style='background-color: #dd4b39;'
              href='#!'
              role='button'
              style={{ backgroundColor: '#dd4b39' }}>
              <i class='fab fa-google'></i>
            </a>

            <a
              class='btn btn-primary btn-floating m-1'
              //   style='background-color: #ac2bac;'
              href='#!'
              role='button'
              style={{ backgroundColor: '#ac2bac' }}>
              <i class='fab fa-instagram'></i>
            </a>

            <a
              class='btn btn-primary btn-floating m-1'
              //   style='background-color: ;'
              href='#!'
              role='button'
              style={{ backgroundColor: '#0082ca' }}>
              <i class='fab fa-linkedin-in'></i>
            </a>
            <a
              class='btn btn-primary btn-floating m-1'
              //   style='background-color: #333333;'
              href='#!'
              role='button'
              style={{ backgroundColor: '#333333' }}>
              <i class='fab fa-github'></i>
            </a>
          </section>
        </div>

        <div class='text-center p-3'>
          <span>© 2022 Copyright </span>
          <span>
            <Link class='text-white text-decoration-none' to='/contact-us'>
              Exam Portal
            </Link>
          </span>
        </div>
      </footer>
    </div>
  )
}

export default Footer
