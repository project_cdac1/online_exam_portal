import { Link } from 'react-router-dom'

const Sidebar2 = () => {
  return (
    <div className='sidebar'>
      <Link to='#' className='logo'>
        <img src='./images/logo.png' alt='logo' className='logo-img'></img>
      </Link>
      <ul className='nav-links'>
        <li className='nav-links-li'>
          <Link to='#' className='nav-links-link'>
            <i class='fa-solid fa-house nav-links-i'></i>
            <p>Dashboard</p>
          </Link>
        </li>
        <li className='nav-links-li'>
          <Link to='#' className='nav-links-link'>
            <i class='fa-solid fa-house nav-links-i'></i>
            <p>Dashboard</p>
          </Link>
        </li>
        <li className='nav-links-li'>
          <Link to='#' className='nav-links-link'>
            <i class='fa-solid fa-house nav-links-i'></i>
            <p>Dashboard</p>
          </Link>
        </li>
        <div className='active'></div>
      </ul>
    </div>
  )
}

export default Sidebar2
