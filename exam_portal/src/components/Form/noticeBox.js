const NoticeBox = (props) => {
  const { title, listType, list } = props

  return (
    <div className='notice'>
      <span className='fw-bold fs-3'>
        <i className='fa-solid fa-circle-exclamation'></i>
        <span> {title}</span>
      </span>
      <hr />
      <ol type={listType}>
        {list.map((l) => {
          return <li>{l.data}</li>
        })}
        {/* <li>Only Enter Starting and Ending Roll Numbers of Student list.</li>
        <li>All rest details can be entered by Student Itself.</li>
        <li>
          All Students UserID and Password Will be Automatically Generated.
        </li>
        <li>
          Username for Students is his <b>Roll No</b> and Initial Password is{' '}
          <b>student@123</b>.
        </li>
        <li>
          Ask Student to change there initial Password on FirstTime LogIn.
        </li> */}
      </ol>
    </div>
  )
}

// const style = {
//   notice: [(backgroundColor = '#cfe2ff'), (color = '#2e60ab')],
// }

export default NoticeBox
