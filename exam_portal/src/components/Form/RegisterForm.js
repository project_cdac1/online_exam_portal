import { useEffect, useRef, useState } from 'react'
import { Link } from 'react-router-dom'
import { ClientData, ExamAdminData } from '../../Data/sidebarData'
import List from '../list'
import Navbar from '../navbar'
import Sidebar from '../sidebar/sidebar'
import NoticeBox from './noticeBox'

const RegisterForm = (props) => {
  const { title, data, submitButton } = props
  const buttonstyle = 'btn ' + submitButton.color + ' col col-6 mx-auto'
  const notice = props.notice ? props.notice : null

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const [value, setValue] = useState('')
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperHeight - navHeight)
  }, [wrapperHeight, navHeight])

  const style = {
    box: {
      borderStyle: 'solid',
      textAlign: 'center',
    },

    vh: {
      minHeight: restHeight * 0.99,
    },
  }
  return (
    <div ref={wrapperRef} className='vh-100' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        {props.noSidebar ? (
          <div >
            <Navbar />
          </div>
        ) : (
          <Sidebar
            data={!props.examAdmin ? ClientData : ExamAdminData}
            title='Institute Name'
          />
        )}
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 p-3 translogin'>
          <div className='row ps-3 ps-sm-5 pe-3 pe-sm-5 pt-2 pb-2'>
            <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic '>
              {title}
            </div>
            <hr />
            {data.map((d) => {
              if (d.isFullWidth) {
                return (
                  <div className='mb-3 col col-12 col-lg-12'>
                    <label>{d.label}</label>
                    <input
                      className='form-control'
                      type={d.type ? d.type : 'text'}
                      onChange={(e) => d.func(e.target.value)}
                      value={d.value}
                      required={d.required}
                      pattern={d.pattern}
                      
                    />
                    <span className='errmsg'>
                      {d.errorMessage}
                    </span>
                  </div>
                )
              } else if (d.isEmpty) {
                return <div className='mb-3 col col-12 col-lg-6 '></div>
              } else if (d.isDropDown) {
                return (
                  <div className='mb-3 col col-12 col-lg-6 '>
                    <label className='me-3'>{d.label}</label>
                    <select onChange={(e) => d.func(e.target.value)}
                    // required={d.required}
                    // pattern={d.pattern}
                    >
                      {d.listData.map((l) => {
                        return <option value={l}>{l}</option>
                      })}
                      <option
                        value={d.selectedValue ? d.selectedValue : value}
                        selected='selected'
                        >
                        {d.selectedValue}
                      </option>
                    </select>
                    <div>
                      {d.isLink ? (
                        <Link to='/plans'>{d.linkTitle}</Link>
                      ) : (
                        <div></div>
                      )}
                    </div>
                    <span className='errmsg'>
                      {d.errorMessage}
                    </span>
                  </div>

                )
              } else if (d.isRadio) {
                return (
                  <div className='mb-3 col col-12 col-lg-6'>
                    <label>{d.label}</label>
                    <div className='row'>
                      {d.list.map((l) => {
                        return (
                          <div className='col col-3 col-md-4'>
                            <label>
                              <input
                                type='radio'
                                name={d.label}
                                value={l}
                                onChange={(e) => d.func(e.target.value)}
                              />
                              <span> {l}</span>
                            </label>
                          </div>
                        )
                      })}
                    </div>
                  </div>
                )
              } else {
                return (
                  <div className='mb-3 col col-12 col-lg-6'>
                    <label>{d.label}</label>
                    <input
                      className='form-control'
                      type={d.type ? d.type : 'text'}
                      onChange={(e) => d.func(e.target.value)}
                      value={d.value}
                      required={d.required}
                      pattern={d.pattern}
                    />
                    <span className='errmsg'>
                      {d.errorMessage}
                    </span>
                  </div>
                )
              }
            })}
            <button className={buttonstyle} onClick={submitButton.func}>
              {submitButton.title}
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RegisterForm
