import { useEffect, useRef, useState } from 'react'
import { StudentData } from '../../Data/sidebarData'
import Sidebar from '../sidebar/sidebar'
import NoticeBox from './noticeBox'

const StartExamFormBox = (props) => {
  const { title, data, submitButton } = props
  const buttonstyle = 'btn ' + submitButton.color + ' col col-4 mx-auto'
  const notice = props.notice ? props.notice : null

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperHeight - navHeight)
  }, [wrapperHeight, navHeight])

  const style = {
    box: {
      borderStyle: 'solid',
      textAlign: 'center',
    },

    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  return (
    <div ref={wrapperRef} className='vh-100 bgstudent'>
      <div ref={navRef}>
        <Sidebar data={StudentData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 p-3 translogin'>
          <div className='row ps-3 ps-sm-5 pe-3 pe-sm-5 pt-2 pb-2'>
            <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic '>
              {title}
            </div>

            <hr />
            {notice != null ? (
              <NoticeBox
                title={notice.title}
                listType={notice.listType}
                list={notice.list}
              />
            ) : (
              <div>ABC</div>
            )}

            {data.map((d) => {
              return (
                <div className='mb-3 col col-12 col-lg-6'>
                  <input
                    type='checkbox'
                    className='form-check-input'
                    onChange={(e) => d.func(e.target.value)}></input>
                  <label>{d.label}</label>
                </div>

                // <div className='mb-3 col col-12 col-lg-6'>
                //   <label>{d.label}</label>
                //   <input
                //     className='form-control'
                //     type='text'
                //     onChange={(e) => d.func(e.target.value)}
                //   />
                // </div>
              )
            })}
            <div className='row'>
              <button className={buttonstyle} onClick={submitButton.func}>
                {submitButton.title}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default StartExamFormBox
