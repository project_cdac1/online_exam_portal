import axios from "axios"
import { toast } from "react-toastify"
import "../index.css"
import config from "../config"
import { useEffect } from "react"
import { useState } from "react"
const PlansBox = () => {
  // const [planName,setPlanName] =useState('')
  // const [noOfQues,setNoOfQues] = useState()
  // const [price, setPrice] = useState()
  const [planList, setPlanList] = useState([])
  const getPlanList= ()=>{
    axios
      .get(config.serverURL + '/subscription/get_sub_details',{})
      .then((response)=>{
        const result = response.data
        setPlanList(result)
        console.log(result)
       // toast.success("Subscription list is fetched successfully")
      })
      .catch((error)=>{
        console.log(error)
      })
  }
  useEffect(() => {
    getPlanList()
  },[])
  return (
    <div className="p-2">
      <div className="col-12">
        <h3 className="title" style={{ color: "#f7b011" }}>
         Subscription Plans
        </h3>
      </div>
      <div style={{ marginTop: 40 }}>
        <div className="row">
          {planList.map((p)=>{
            return (
          <div className="col-sm-3">
            <div className="card, cardstyle rounded-3 py-0 py-sm-2 translogin">
              <div className="card-body" style={{ marginTop: 20 }}>
                <h6 className="card-title, cardtext1">{p.planName}</h6>
                <h5 className="cardtext2"> &#8377;{p.price}/- </h5>

                <div className="col-12">
                  <p className="cardtext3">
                    <ul className="ul">
                      <li> Maximum {p.noOfExams} exams allowed</li>
                      <li> Full time support available</li>
                    </ul>
                  </p>
                </div>
                <div className="form-row py-5 pt-5">
                  <div className="offset-1 col-lg-6, btn2_div">
                    {/* <button className="btn2">Select</button> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
            )
          })}
         
        </div>
      </div>
    </div>
  )
}

export default PlansBox
