import List from '../components/list'
import { Link } from 'react-router-dom'
const Navbar = () => {
  return (
    <div className='col col-12 trans'>
      <nav
        className='navbar navbar-expand-lg'
      >
        <div className='container-fluid'>
          {/* <Link
            to='#'
            className='navbar-brand ms-4 text-white fs-2 fw-bold'
            id='home'>
              
            Arjuna
          </Link> */}
           <Link to='#' className='logo2'>
              <img src='../images/my_logo.png' alt='logo' className='logo2'></img>
            </Link>
          <div className='justify-content-end'>
            <button
              className='navbar-toggler shadow-none'
              style={{ marginLeft: 140 }}
              type='button'
              data-bs-toggle='collapse'
              data-bs-target='#navbarNavAltMarkup'
              aria-controls='navbarNavAltMarkup'
              aria-expanded='false'
              aria-label='Toggle navigation'>
              <span className='navbar-toggler-icon'></span>
            </button>
            <div
              className='collapse navbar-collapse me-5'
              id='navbarNavAltMarkup'>
              <ul className='navbar-nav'>
                <List title='Home' to='/'></List>
                <List title='Plans' to='/plans'></List>
                <List title='Contact Us' to='/contact-us'></List>
                <li class='nav-item dropdown'>
                  <Link
                    class='nav-link dropdown-toggle menu fs-5 fw-bold hoverEffect px-3'
                    to='#'
                    id='navbarDropdown'
                    role='button'
                    data-bs-toggle='dropdown'
                    aria-expanded='false'>
                    LogIn
                  </Link>
                  <ul
                    class='dropdown-menu dropdown-menu-center'
                    aria-labelledby='navbarDropdown'>
                    <li>
                      <Link class='dropdown-item dropSub' to='/login-admin'>
                        Admin
                      </Link>
                    </li>
                    <li>
                      <Link class='dropdown-item dropSub' to='/login-client'>
                        Client
                      </Link>
                    </li>
                    <li>
                      <Link class='dropdown-item dropSub' to='/login-examadmin'>
                        Exam Admin
                      </Link>
                    </li>
                    <li>
                      <Link class='dropdown-item dropSub' to='/login-student'>
                        Student
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default Navbar
