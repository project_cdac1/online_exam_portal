import React from 'react'
import { useState } from 'react'
import { Navigate } from 'react-router-dom'
import { toast } from 'react-toastify'
const PrivateClientComponent = (props) => {
  // const [jwt, setJwt] = useState(
  //   JSON.parse(sessionStorage.getItem('userInfo')).jwt
  // )
  // // const [user, setUser] = useState(
  //   JSON.parse(sessionStorage.getItem('userInfo')).client
  // )
  const Component = props.Component
  // const msg = toast.warning('Test')
  return JSON.parse(sessionStorage.getItem('userInfo')).jwt &&
    JSON.parse(sessionStorage.getItem('userInfo')).client ? (
    <Component />
  ) : (
    // toast.('Login First ...')
    <>
      {toast.warning('Login First')}
      <Navigate to={'/login-client'} />
    </>
  )
}
const PrivateExamAdminComponent = (props) => {
  // const [jwt, setJwt] = useState(
  //   JSON.parse(sessionStorage.getItem('userInfo')).jwt
  // )
  // // const [user, setUser] = useState(
  //   JSON.parse(sessionStorage.getItem('userInfo')).client
  // )
  const Component = props.Component
  // const msg = toast.warning('Test')
  return JSON.parse(sessionStorage.getItem('userInfo')).jwt &&
    JSON.parse(sessionStorage.getItem('userInfo')).examAdmin ? (
    <Component />
  ) : (
    // toast.('Login First ...')
    <>
      {toast.warning('Login First')}
      <Navigate to={'/login-examadmin'} />
    </>
  )
}
const PrivateStudentComponent = (props) => {
  // const [jwt, setJwt] = useState(
  //   JSON.parse(sessionStorage.getItem('userInfo')).jwt
  // )
  // // const [user, setUser] = useState(
  //   JSON.parse(sessionStorage.getItem('userInfo')).client
  // )
  const Component = props.Component
  // const msg = toast.warning('Test')
  return JSON.parse(sessionStorage.getItem('userInfo')).jwt &&
    JSON.parse(sessionStorage.getItem('userInfo')).student ? (
    <Component />
  ) : (
    // toast.('Login First ...')
    <>
      {toast.warning('Login First')}
      <Navigate to={'/login-student'} />
    </>
  )
}
const PrivateAdminComponent = (props) => {
  // const [jwt, setJwt] = useState(
  //   JSON.parse(sessionStorage.getItem('userInfo')).jwt
  // )
  // // const [user, setUser] = useState(
  //   JSON.parse(sessionStorage.getItem('userInfo')).client
  // )
  const Component = props.Component
  // const msg = toast.warning('Test')
  return JSON.parse(sessionStorage.getItem('userInfo')).jwt &&
    JSON.parse(sessionStorage.getItem('userInfo')).admin ? (
    <Component />
  ) : (
    // toast.('Login First ...')
    <>
      {toast.warning('Login First')}
      <Navigate to={'/login-admin'} />
    </>
  )
}
export {PrivateClientComponent,PrivateExamAdminComponent,PrivateStudentComponent,PrivateAdminComponent}
