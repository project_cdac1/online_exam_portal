import { useEffect } from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'

const Timer = (props) => {
  //   let t = '00:02:00'
  //const [t, setT] = useState('')
  const [hr, setHr] = useState(0)
  const [min, setMin] = useState(0)
  const [sec, setSec] = useState(59)
  const [interv, setInterv] = useState()
  const examDuration = sessionStorage.getItem('selectedExamDuration')
  //   const start = () => {
  //     run()
  //     // setInterval(run(), 1000)
  //   }
  var timer
  useEffect(() => {
    let t = '00:02:00'
    console.log('test')

    let second = sec
    let minute = (examDuration % 60) - 1
    let hour = parseInt(examDuration / 60)
    setHr(parseInt(examDuration / 60))
    setMin((examDuration % 60) - 1)

    // setT('00:02:00')
    timer = setInterval(() => {
      second--
      if (second <= 0) {
        //   console.log(minute)
        if (minute > 0) {
          // console.log('sec0')
          second = 59
          setMin(minute - 1)
          minute--
          // console.log(minute)
        } else if (minute == 0) {
          // console.log('min0')
          if (hour > 0) {
            setHr(hour - 1)
            setMin(59)
            minute = 59
          } else {
            // toast.success('Response submitted Successfully ...')
            clearInterval(timer)
            {
              props.submit()
            }
          }
        }
      }
      return setSec(second)
    }, 1000)
  }, [])
  //   useEffect(() => {
  //     // run()
  //   })

  const navigate = useNavigate()

  // let second = sec
  // let minute = min
  // let hour = hr
  // const run = () => {
  //   second--
  //   if (second <= 0) {
  //     //   console.log(minute)
  //     if (minute > 0) {
  //       // console.log('sec0')
  //       second = 59
  //       setMin(minute - 1)
  //       minute--
  //       // console.log(minute)
  //     } else if (minute == 0) {
  //       // console.log('min0')
  //       if (hour > 0) {
  //         setHr(hour - 1)
  //         setMin(59)
  //         minute = 59
  //       } else {
  //         toast.success('Response submitted Successfully ...')
  //         clearInterval(timer)
  //         navigate('/st-home')

  //         //   return
  //       }
  //     }
  //   }
  //   return setSec(second)
  // }
  return (
    <div className='col col-12 text-end px-5 pt-3 fw-bolder fs-3 stickyNav'>
      {hr > 9 ? hr : '0' + hr} : {min > 9 ? min : '0' + min}:{' '}
      {sec > 9 ? sec : '0' + sec}
    </div>
  )
}

export default Timer
