const Input = (props) => {
  const { title, type, onChange } = props
  return (
    <div className='mb-3 col col-12 col-lg-12'>
      <label className='mb-2'>{title}</label>
      <input
        style={{ borderStyle: 'solid' }}
        className='form-control mb-2 border-dark'
        type={type ? type : 'text'}
        onChange={onChange}></input>
    </div>
  )
}

export default Input
