const ClientData = {
  list: [
    {
      icon: 'fa-solid fa-house-user',
      title: 'Home',
      to: '/client/client-home',
    },
    {
      isHR: true,
    },
    {
      icon: 'fa-solid fa-chalkboard-user',
      title: 'Add Exam Admin',
      to: '/client/create-exam-admin',
    },
    {
      icon: 'fa-solid fa-graduation-cap',
      title: 'Add Students',
      to: '/client/add-students',
    },
    {
      isHR: true,
    },
    {
      icon: 'fa-solid fa-user-xmark',
      title: 'Manage Exam Admin',
      to: '/client/manage-exam-admin',
    },
    {
      icon: 'fa-solid fa-calendar-xmark',
      title: 'Manage Exam',
      to: '/client/manage-exam',
    },

    {
      icon: 'fa-solid fa-trash-can',
      title: 'Manage Student',
      to: '/client/manage-student',
    },
    {
      isHR: true,
    },
    {
      icon: 'fa-solid fa-pen-to-square',
      title: 'Edit Profile',
      to: '/client/edit-profile',
    },
    {
      icon: 'fa-solid fa-key',
      title: 'Change Password',
      to: '/client/edit-password',
    },
  ],

  title: 'Arjuna Online Examination Portal',
}

const StudentData = {
  list: [
    {
      icon: 'fa-solid fa-house-user',
      title: 'Home',
      to: '/student/home',
    },

    {
      icon: 'fa-solid fa-calendar',
      title: 'Important Dates',
      to: '/st-impDates',
    },
    {
      icon: 'fa-solid fa-laptop-code',
      title: 'Select Exam',
      to: '/student/select-exam',
    },
    {
      icon: 'fa-regular fa-file',
      title: 'Downloads',
      to: '/student/result',
    },
    {
      isHR: true,
    },
    {
      icon: 'fa-solid fa-key',
      title: 'Change Password',
      to: '/student/edit-password',
    },
    {
      icon: 'fa-solid fa-pen-to-square',
      title: 'Edit Profile',
      to: '/student/edit-profile',
    },
  ],
  title: 'Arjuna Online Examination Portal',
}

const ExamAdminData = {
  list: [
    {
      icon: 'fa-solid fa-house-user',
      title: 'Home',
      to: '/exam-admin/home',
    },
    {
      isHR: true,
    },
    {
      icon: 'fa-solid fa-file-circle-check',
      title: 'Create Exam',
      to: '/Create-Exam',
    },
    {
      icon: 'fa-solid fa-pen',
      title: 'Add Marking Scheme',
      to: '/Marking-Scheme',
    },

    {
      icon: 'fa-solid fa-file-circle-plus',
      title: 'Add Question Paper',
      to: '/select-exam/create',
    },
    {
      icon: 'fa-solid fa-pen-to-square',
      title: 'Update Question',
      to: '/select-exam/update',
    },
    {
      icon: 'fa-solid fa-eye',
      title: 'Show Exam List',
      to: '/show-exam-list',
    },
    {
      isHR: true,
    },
    {
      icon: 'fa-solid fa-pen-to-square',
      title: 'Edit Profile',
      to: '/exam_admin/edit_profile',
    },
    {
      icon: 'fa-solid fa-key',
      title: 'Change Password',
      to: '/exam_admin/edit-password',
    },
  ],
  title: 'Arjuna Online Examination Portal',
}

const AdminData = {
  list: [
    {
      icon: 'fa-solid fa-house-user',
      title: 'Home',
      to: '/admin-home',
    },

    {
      icon: 'fa-solid fa-pen-to-square',
      title: 'Edit Subscription',
      to: '/select-subscription',
    },
    {
      icon: 'fa-solid fa-laptop-code',
      title: 'Client List',
      to: '/client-list',
    },
  ],
  title: 'Arjuna Online Examination Portal',
}

export { ClientData, StudentData, ExamAdminData, AdminData }
