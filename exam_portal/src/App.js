import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './pages/Home/home'
import ClientHome from './pages/Client/clientHome'
import ContactUs from './pages/Home/contactUs'
import Plans from './pages/Home/plans'
import StudentHome from './pages/student/st-home'
import CreateExam from './pages/Exam_admin/createExam'
import MarkingScheme from './pages/Exam_admin/markingScheme'
import AddQuestion from './pages/Exam_admin/AddQuestion'
import ImpDates from './pages/student/st-impDates'
import McqTest from './pages/student/st-mcq'
import 'react-toastify/dist/ReactToastify.css'
import RegisterClient from './pages/Register_client'
import Signin from './pages/login'
import RegisterExamAdmin from './pages/Client/register_exam_admin'
import RegisterStudent from './pages/Client/register_student'
import EditClient from './pages/Client/EditClient'
import ManageStudent from './pages/Client/manageStudent'
import ManageExam from './pages/Client/manageExam'
import ManageExamAdmin from './pages/Client/manageExamAdmin'
import EditExamAdmin from './pages/Exam_admin/editExamAdmin'
import ExamList from './pages/Exam_admin/ExamList'
import CreateQuestionPaper from './pages/Exam_admin/createQuestionPaper'
import ForgotPassword from './pages/forgotPassword'
import ExamAdminHome from './pages/Exam_admin/examAdminHome'
import UpdateQuestion from './pages/Exam_admin/updateQuestion'
import SelectExam from './pages/student/st-selectExam'
import StudentResult from './pages/student/student-result'
import EditPassword from './pages/student/st-editPassword'
import EditStudentProfile from './pages/student/st-editProfile'
import AdminHome from './pages/Admin/admin-home'
import ClientList from './pages/Admin/client-list'
import SelectSubscription from './pages/Admin/select-subscription'
import EditSubscription from './pages/Admin/edit-subscription'
import { PrivateClientComponent, PrivateExamAdminComponent, PrivateStudentComponent, PrivateAdminComponent } from './components/privateComponent'
import { ToastContainer } from 'react-toastify'
import StartExam from './pages/student/st-startExam'

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route
            path='/client/client-home'
            element={<PrivateClientComponent Component={ClientHome} />}
          />

          <Route
            path='/client/manage-exam'
            element={<PrivateClientComponent Component={ManageExam} />}
          />
          <Route
            path='/client/manage-student'
            element={<PrivateClientComponent Component={ManageStudent} />}
          />

          <Route
            path='/client/manage-exam-admin'
            element={<PrivateClientComponent Component={ManageExamAdmin} />}
          />
          <Route path='/contact-us' element={<ContactUs />} />
          <Route
            path='/login-client'
            element={<Signin role='Client' to='/client/client-home' />}
          />
          <Route
            path='/login-student'
            element={<Signin role='Student' to='/student/home' />}
          />
          <Route
            path='/login-examadmin'
            element={<Signin role='Exam_Admin' to='/exam-admin/home' />}
          />
          <Route path='/plans' element={<Plans />} />
          <Route path='/student/home' element={<PrivateStudentComponent Component={StudentHome} />} />
          <Route
            path='/student/edit-profile'
            element={<PrivateStudentComponent Component={EditStudentProfile} />}
          />
          <Route
            path='/student/edit-password'
            element={<EditPassword role='student' to='/login-student' />}
          />
          <Route
            path='/client/edit-password'
            element={<EditPassword role='client' to='/login-client' />}
          />
          <Route
            path='/exam_admin/edit-password'
            element={<EditPassword role='exam_admin' to='/login-examadmin' />}
          />
          <Route path='/st-impDates' element={<PrivateStudentComponent Component={ImpDates} />} />
          <Route path='/st-startExam' element={<PrivateStudentComponent Component={StartExam} />} />
          <Route path='/student/select-exam' element={<PrivateStudentComponent Component={SelectExam} />} />
          <Route path='exam-admin/home' element={<PrivateExamAdminComponent Component={ExamAdminHome} />} />
          <Route path='/Create-Exam' element={<PrivateExamAdminComponent Component={CreateExam} />} />
          <Route path='/Marking-Scheme' element={<PrivateExamAdminComponent Component={MarkingScheme} />} />
          <Route path='/Add-Question' element={<PrivateExamAdminComponent Component={AddQuestion} />} />
          <Route path='/McqTest' element={<PrivateStudentComponent Component={McqTest} />} />
          <Route path='/client/register' element={<RegisterClient />} />
          <Route
            path='/client/create-exam-admin'
            element={<PrivateClientComponent Component={RegisterExamAdmin} />}
          />
          <Route
            path='/client/add-students'
            element={<PrivateClientComponent Component={RegisterStudent} />}
          />


          <Route
            path='/client/edit-profile'
            element={<PrivateClientComponent Component={EditClient} />}
          />
          <Route path='/exam_admin/edit_profile' element={<PrivateExamAdminComponent Component={EditExamAdmin} />} />
          <Route path='/forgot-password' element={<ForgotPassword />} />
          <Route
            path='/select-exam/create'
            element={<ExamList to='create' show={true} />}
          />
          <Route
            path='/select-exam/update'
            element={<ExamList to='update'  show={true}/>}
          />
          <Route path='/show-exam-list' element={<PrivateExamAdminComponent Component={ExamList} />} />
          <Route
            path='/exam-admin/update-question'
            element={<PrivateExamAdminComponent Component={UpdateQuestion} />}
          />
          <Route
            path='/create-question-paper'
            element={<PrivateExamAdminComponent Component={CreateQuestionPaper} />}
          />
          <Route path='/student/result' element={<PrivateStudentComponent Component={StudentResult} />} />
          <Route path='/admin-home' element={<PrivateAdminComponent Component={AdminHome} />} />
          <Route path='/client-list' element={<PrivateAdminComponent Component={ClientList} />} />
          <Route path='/select-subscription' element={<PrivateAdminComponent Component={SelectSubscription} />} />
          <Route path='/edit-subscription' element={<PrivateAdminComponent Component={EditSubscription} />} />
          <Route
            path='/login-admin'
            element={<Signin role='Admin' to='/admin-home' />}
          />
        </Routes>
        <ToastContainer position='top-center' autoClose={600} />
      </BrowserRouter>
    </div>
  )
}

export default App
