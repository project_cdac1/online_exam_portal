import StFormBox from "../../components/Form/st-formbox"
import { useState } from "react"
import { useNavigate } from "react-router-dom"
import { toast } from "react-toastify"
import StartExamFormBox from "../../components/Form/StartExamFormBox"
const StartExam = () => {
   const [check,setCheck] = useState(false)
   const navigate = useNavigate()
   
   const start=()=>{
     //console.log(sessionStorage.getItem('selectedExamId'))
    if(check === false)
    toast.error('please select checkbox')
    else {
      toast.success('start exam')
      navigate('/McqTest')
    }
  }
    const data = [
      {
        label: 'I have read all the instructions',
        func : setCheck, 
      },
     
    ]
 
    const submitButton = {
        title: 'Start',
        color: 'btn-primary',
        func : start
      }
  
    
    const notice = {
        title: 'Instructions : ',
        listType: '1',
        list: [
          {
            data: 'No cell phones or other secondary devices in the room or test area',
          },
          {
            data: 'Your desk/table must be clear or any materials except your test-taking device',
          },
          {
            data: 'The testing room must be well-lit and you must be clearly visible',
          },
          {
            data: 'Do not leave the camera',
          },
          {
            data: 'No dual screens/monitors',
          },
        ],
      }
    
  return (<div className="bgstudent">
    <StartExamFormBox
      data={data}
      submitButton={submitButton}
      title='Exam Page'
      notice={notice}
    />
    </div>
  )
}

export default StartExam