import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import Sidebar from '../../components/sidebar/sidebar'
import { StudentData } from '../../Data/sidebarData'
import config from '../../config'
import { toast } from 'react-toastify'

const ImpDates = () => {
  const [schedule, setSchedule] = useState([])
  //const schedule = 
  //   {
  //     exam_Name: 'Exam1',
  //     exam_Date:'',
  //     exam_Time:'',
  //     result_Date:'',
      
  //   },
  //   {
  //     exam_Name: 'Exam2',
  //     exam_Date:'',
  //     exam_Time:'',
  //     result_Date:'',
  //   },
  //   {
  //     exam_Name: 'Exam3',
  //     exam_Date:'',
  //     exam_Time:'',
  //     result_Date:'',
  //   },
  // ]
  // function deleteExamAdmin(ex_id) {
  //  console.log(ex_id)
  //}

  // const deleteHome = (id) => {
    const student=JSON.parse(sessionStorage.getItem('userInfo'))
  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }
  const clientId= student.student.clientId
  const token = student.jwt
  const getImpDates = ()=>{
    axios.get(config.serverURL + '/student/imp_dates' + `/${clientId}`,{headers: { "Authorization": `Bearer ${token}` }})
    .then((response) => {
     // console.log(response.data)
      setSchedule(response.data)
    })
    .catch((error)=>{
      toast.error("list is empty")
     // console.log(error)
    })
  }
  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    getImpDates()
  },[])

console.log(schedule)
  return (
    <div ref={wrapperRef} className='vh-100 bgstudent'style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={StudentData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-10 boxShadow rounded-3 px-5 py-3 table-responsive text-center translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
            Important Dates
          </div>
          <hr />
          <table className='table border-dark table-hover mt-3'>
            <thead>
              <tr>
                <th>Exam Name</th>
                <th>Exam Date</th>
                <th>Exam Time</th>
                <th>Result Date</th>
              </tr>
            </thead>
            <tbody className='table-group-divider'>

              {schedule.map((e) => {
                return (
                  <tr>
                    <td>{e.examName}</td>
                    <td>{e.scheduledDate}</td>
                    <td>{e.scheduledTime}</td>
                    <td>{e.resultDate}</td>

                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default ImpDates
