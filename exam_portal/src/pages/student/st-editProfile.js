import axios from 'axios'
import { useEffect } from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import RegisterForm from '../../components/Form/RegisterForm'
import config from '../../config'
import StFormBox from '../../components/Form/st-formbox'

const EditStudentProfile = () => {
  // var subscriptionList
  // var pinCodeList

  const student = JSON.parse(sessionStorage.getItem('userInfo'))
  let address = student.student.address
  const token = student.jwt

  const [name, setName] = useState(student.student.name)
  const [pinCode, setPinCode] = useState(student.student.address.pinCode)
  const [pinCodeList, setPinCodeList] = useState([])
  const [email, setEmail] = useState(student.student.email)
  const [mobile, setMobile] = useState(student.student.mobile)

  const [addressLine1, setAddress1] = useState(student.student.addressLine1)
  const [addressLine2, setAddress2] = useState(student.student.addressLine2)
  const [dob, setDOB] = useState(student.student.dob)
  const [gender, setGender] = useState()
  const [rollNo, setRollNo] = useState(student.student.rollNo)
  const [clientId, setClientId] = useState(student.student.clientId)

  const navigate = useNavigate()

  const editProfile = () => {
    // console.log({ gender })
    // console.log({ dob })

    if (name.length === 0) {
      toast.error('please enter org name')
    } else if (pinCode.length === 0) {
      toast.error('please enter pincode')
    } else if (email.length === 0) {
      toast.error('please enter email')
    } else if (mobile.length === 0) {
      toast.error('please enter phone number')
    } else if (addressLine1.length === 0) {
      toast.error('please enter address')
    } else if (dob.length === 0) {
      toast.error('please enter Date of Birth')
    } else if (gender.length === 0) {
      toast.error('please select Gender')
    } else if (rollNo === null) {
      toast.error('please Enter roll No')
    } else {
      const body = {
        id: student.student.id,
        name,
        pinCode,
        email,
        mobile,
        addressLine1,
        addressLine2,
        clientId,
        dob,
        gender,
        rollNo,
      }
      axios
        .put(config.serverURL + '/student/update_student', body,{headers: { "Authorization": `Bearer ${token}` }})
        .then((response) => {
          //const result = response.data
          const result = JSON.stringify(response.data)
          //console.log(result)
          sessionStorage.setItem('userInfo', result)
          toast.success('Your Information Updated Successfully')
          navigate('/login-student')
        })
        .catch((error) => {
          toast.error('error')
        })
    }
  }

  const getPincodeList = () => {
    axios
      .get(config.serverURL + '/address/get_pincode_list',{headers: { "Authorization": `Bearer ${token}` }})
      .then((response) => {
        var result = response.data
        setPinCodeList(result)
        // pinCodeList = result
        // if (result['status'] === 'error') {
        //   toast.error('No List Found')
        // } else {
        // }
      })
  }

  useEffect(() => {
    // const ids = i.id
    // console.log(i.id)
    //setStudentId(student.id)
    getPincodeList()
  }, [])

  const data = [
    {
      label: 'Name',
      func: setName,
      isFullWidth: true,
      value: name,
    },
    {
      label: 'AddressLine1',
      func: setAddress1,
      value: addressLine1,
    },
    {
      label: 'AddressLine2',
      func: setAddress2,
      value: addressLine2,
    },
    {
      label: 'PinCode',
      func: setPinCode,
      isDropDown: true,
      listData: pinCodeList,
      //value: address.pinCode,
      selectedValue: address.pinCode,
    },
    {
      label: 'Gender',
      func: setGender,
      isRadio: true,
      list: ['MALE', 'FEMALE', 'OTHER'],
      value: gender,
    },
    {
      label: 'Date of Birth',
      func: setDOB,
      type: 'date',
      value: dob,
    },
    {
      label: 'Email',
      func: setEmail,
      value: email,
    },
    {
      label: 'Contact Number',
      func: setMobile,
      type: 'number',
      value: mobile,
    },
    {
      label: 'Roll No',
      func: setRollNo,
      type: 'number',
      value: rollNo,
    },
  ]

  const submitButton = {
    title: 'Edit Student',
    color: 'btn-success',
    func: editProfile,
  }

  return (
    <div className='bgstudent'>
    <StFormBox data={data} submitButton={submitButton} title='Edit-Profile' />
    </div>
  )
}

export default EditStudentProfile
