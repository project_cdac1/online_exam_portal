import React, { useState, useEffect } from 'react'
import NavbarMcq from './NavbarMcq'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import config from '../../config'
import { toast } from 'react-toastify'
import Timer from '../../components/timer'
const McqTest = () => {
  const navigate = useNavigate()
  const examName = sessionStorage.getItem('selectedExamName').toUpperCase()
  const student=JSON.parse(sessionStorage.getItem('userInfo'))
  const token = student.jwt
  const getQuestionList = () => {
    const id = sessionStorage.getItem('selectedExamId')
    axios
      .get(config.serverURL + `/exam/get_questions/${id}`,{headers: { "Authorization": `Bearer ${token}` }})
      .then((response) => {
        console.log(response.data)
        // setTest(JSON.stringify(QuestionData))
        // setQuestionData(response.data)
        setTest(response.data)
      })
      .catch((error) => {
        //console.log(error)
        toast.error("question list is empty")
      })
  }

  useEffect(() => {
    getQuestionList()
    console.log(test)
  }, [])

  const [test, setTest] = useState([])

  const updateAns = (id, answer) => {
    console.log({ id })
    console.log({ answer })
    setTest((current) =>
      current.map((ques) => {
        if (ques.id === id) {
          console.log(answer)
          return { ...ques, ans: answer }
        }
        return ques
      })
    )
  }

  const submit = () => {
    const body = []
    test.map((q) => {
      body.push({
        studentId: student.student.id,
        questionId: q.id,
        response: q.ans ? q.ans : "",
      })
    })
    console.log({ body })
    axios
      .post(config.serverURL + '/student/submit_response', body, {headers: { "Authorization": `Bearer ${token}` }})
      .then((response) => {
        const result = response.data
        //toast.success(result.message)

        navigate('/student/home')
      })
      .catch((error) => {
        //toast.error(error.response.data.message)
      })
  }

  return (
    <div className='vh-100 bgmcq' style={{ overflow: 'auto' }}>
      <div className='fw-bold col col-12 mx-auto fs-1 text-center text-white fst-italic pb-3 mt-3 border-bottom border-primary border-3 '>
        {examName}
        {/* <Timer submit={submit} /> */}
      </div>

      <div>
        {test.map((q) => {
          return (
            <div className='col col-11 col-lg-8 boxShadow rounded-3 p-3 mb-3 mt-3 mx-auto transmcq'>
              <p className='fw-bold'>QuestionNo: {q.quesNo}</p>
              <p className='fw-bold fs-4'>Question: {q.question}</p>
              {/* <p>A:{q.OptionA}</p>
              <p>B:{q.OptionB}</p>
              <p>C:{q.OptionC}</p>
              <p>D:{q.OptionD}</p> */}
              {
                <div class='form-check'>
                  <input
                    class='form-check-input'
                    type='radio'
                    name={q.id}
                    id={q.id}
                    onClick={() => updateAns(q.id, 'optA')}
                  />
                  <label class='form-check-label fs-5' for='flexRadioDefault1'>
                    A: {q.optA}
                  </label>
                </div>
              }
              {
                <div class='form-check'>
                  <input
                    class='form-check-input'
                    type='radio'
                    name={q.id}
                    id={q.id}
                    onClick={() => updateAns(q.id, 'optB')}
                  />
                  <label class='form-check-label fs-5' for='flexRadioDefault1'>
                    B: {q.optB}
                  </label>
                </div>
              }
              {
                <div class='form-check'>
                  <input
                    class='form-check-input'
                    type='radio'
                    name={q.id}
                    id={q.id}
                    onClick={() => updateAns(q.id, 'optC')}
                  />
                  <label class='form-check-label fs-5' for='flexRadioDefault1'>
                    C: {q.optC}
                  </label>
                </div>
              }
              {
                <div class='form-check'>
                  <input
                    class='form-check-input'
                    type='radio'
                    name={q.id}
                    id={q.id}
                    onClick={() => updateAns(q.id, 'optD')}
                  />
                  <label class='form-check-label fs-5' for='flexRadioDefault1'>
                    D: {q.optD}
                  </label>
                </div>
              }
            </div>
          )
        })}
      </div>
      <div onCl className='col col-11 col-lg-4 p-3 mb-3 mt-3 mx-auto'>
        <button className='btn btn-success col col-12' onClick={submit}>
          Submit
        </button>
      </div>
    </div>
  )
}

export default McqTest
