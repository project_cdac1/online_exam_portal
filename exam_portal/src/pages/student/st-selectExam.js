import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import config from '../../config'
import { ClientData, StudentData } from '../../Data/sidebarData'

const SelectExam = () => {
  const [list, setList] = useState([])
  const [questioList, setQuestionList] = useState([])
  const navigate = useNavigate()
  let data
  //   function toggleStatus(id) {
  //     axios
  //       .put(config.serverURL + '/exam/toggle_exam_status' + `/${id}`, {})
  //       .then((response) => {
  //         const data = JSON.parse(sessionStorage.getItem('userInfo'))
  //         getList(data.id)
  //         toast.success(response.data.message)
  //       })
  //       .catch((error) => {
  //         toast.error('Failed to load exam details')
  //       })
  //   }

  // const deleteHome = (id) => {

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }
  const student=JSON.parse(sessionStorage.getItem('userInfo'))
  const token = student.jwt
  const clientId= student.student.clientId
  const studentId= student.student.id
  


  const getList = (id) => {
    // console.log('test')
    axios
      .get(config.serverURL + '/exam/get_exam_list_for_student' + `/${id}`, {headers: { "Authorization": `Bearer ${token}` }})
      .then((response) => {
        const result = response.data
        console.log(result)
        setList(result)
        //console.log(result)
      })
      .catch((response) => {
        toast.error('Failed to load exam details')
      })
  }
  const selectExam = (examId, examName, examDuration) => {
    sessionStorage.setItem('selectedExamId', examId)
    sessionStorage.setItem('selectedExamName', examName)
    sessionStorage.setItem('selectedExamDuration', examDuration)

    navigate('/st-startExam')
    //navigate('/st-startExam'+`/${examId}`)
    // axios.get(config.serverURL + '/exam/get_questions' + `/${examId}`)
    //     .then((response) => {
    //       console.log(response.data)
    //      setQuestionList(response.data)

    //      navigate('/st-startExam')
    //     })
    //     .catch((error)=>{
    //       console.log(error)
    //     })
  }
  useEffect(() => {
    
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    // getList(clientId)
    getList(studentId)
  }, [])

  return (
    <div ref={wrapperRef} className='vh-100 bgstudent' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={StudentData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 px-5 py-3 text-center table-responsive translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
            Select Exam
          </div>
          <hr />
          <table className='table table-hover table-secondary'>
            <thead>
              <tr>
                <th>Exam Name</th>
                <th>Scheduled Date</th>
                <th>Scheduled Time</th>
                <th>Exam Duration</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {list.map((l) => {
                return (
                  <tr>
                    <td>{l.examName}</td>

                    <td>{l.scheduledDate}</td>
                    <td>{l.scheduledTime}</td>
                    <td>{l.examDuration} min</td>
                    <td>
                      {l.examStatus ? (
                        <button
                          onClick={() =>
                            selectExam(l.id, l.examName, l.examDuration)
                          }
                          className='btn btn-success w-100'>
                          Select
                        </button>
                      ) : (
                        <button
                          // onClick={() => toast('Exam Not Yet Started')}
                          className='btn btn-danger w-100'
                          disabled>
                          Select
                        </button>
                      )}
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default SelectExam
