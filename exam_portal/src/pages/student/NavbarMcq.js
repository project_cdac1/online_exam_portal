import React from "react"
import { Link } from "react-router-dom"
function NavbarMcq() {
  return (
    <div className="col col-12 stickyNav">
      <nav
        className="navbar navbar-expand-lg"
        style={{
          background: "linear-gradient(110deg, #ff6464 15%, #5ac8fa 35%)",
        }}
      >
        <div className="container-fluid">
          <Link
            to="#"
            className="navbar-brand ms-4 text-white fs-2 fw-bold"
            id="home"
          >
            Arjuna
          </Link>
          <div className="justify-content-end">
            <button
              className="navbar-toggler shadow-none"
              style={{ marginLeft: 140 }}
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse me-5"
              id="navbarNavAltMarkup"
            ></div>
          </div>
        </div>
      </nav>
    </div>
  )
}
export default NavbarMcq
