import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import Sidebar from '../../components/sidebar/sidebar'
import { StudentData } from '../../Data/sidebarData'
import config from '../../config'
import { toast } from 'react-toastify'
import { Link } from 'react-router-dom'

const StudentResult = () => {
  const [result, setResult] = useState([])
 
    const student=JSON.parse(sessionStorage.getItem('userInfo'))
  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)
  const name = student.name
  const rollNo = student.rollNo
  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }
  // const getResultPdf = ()=>{
  //   console.log("get result pdf")
    
  //   axios
  //   .get(config.serverURL + '/pdf/generate' + `/${id}`,{headers: { "Authorization": `Bearer ${token}` }})
  //   .then((response)=>{
  //     toast.success("result pdf generated")
  //   })
  //   .catch((error)=>{
  //     console.log(error)
  //   })
  // }
  // axios
  //   .get(config.serverURL + '/user/statusforloan/'+sessionStorage.id, {
  //       headers: { "Authorization": `Bearer ${token}` }
  //   })
  //   .then((response) => {
  //       const result = response.data
  //       console.log(result)
  //       setTotalCount(response.data.length)
  //       setList(result)
  //      // toast.success('Admin List')
  //   })
  //   .catch((error) => {
  //       toast.error('Check details once')
  //   })

  //  useEffect(LoanStatus, [token])
  const token = student.jwt
  const id = student.student.id
  console.log(token)
  console.log(id)
  const getResultList = ()=>{
    axios.get(config.serverURL + '/student/get_student_result' + `/${id}`,{
      headers: { "Authorization": `Bearer ${token}` }
    })
    .then((response) => {
     // console.log(response.data)
     setResult(response.data)
    })
    .catch((error)=>{
      //console.log(error)
      toast.error("student list is empty")
    })
  }
  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
  // getResultPdf()
   getResultList()
    
  },[])

console.log(result)
  return (
    <div ref={wrapperRef} className='vh-100 bgstudent' style={{overflow:'auto'}}>
      <div ref={navRef}>
        <Sidebar data={StudentData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-10 boxShadow rounded-3 px-5 py-3 table-responsive text-center translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
            Final Result
          </div>
          <hr />
          <table className='table border-dark table-hover mt-3'>
            <thead>
              <tr>
                <th>Exam Name</th>
                <th>Marks</th>
                <th>Total Marks</th>
                <th>Grade</th>
              </tr>
            </thead>
            <tbody className='table-group-divider'>

              {result.map((e) => {
                return (
                  <tr>
                    <td>{e.examName}</td>
                    <td>{e.studentMarks}</td>
                    <td>{e.totalMarks}</td>
                    <td>{e.grade}</td>

                  </tr>
                )
              })}
            </tbody>
          </table>
          <a href={`http://3.110.56.213:8090/examPortal/pdf/generate/${id}`}>Download</a>
        {/* <a href=config.serverURL + `/pdf/generate/${student.id}?name=student.name&rollNo=student.rollNo`>Download</a> */}
        </div>
      </div>
    </div>
  )
}

export default StudentResult
