import '../../index.css'
import Sidebar from '../../components/sidebar/sidebar'
import { useState } from 'react'
import { useRef } from 'react'
import { useEffect } from 'react'
import { StudentData, ClientData, ExamAdminData } from '../../Data/sidebarData'
import axios from 'axios'
import { toast } from 'react-toastify'
import { Navigate } from 'react-router-dom'
import config from '../../config'
import { useNavigate } from 'react-router-dom'
const EditPassword = (props) => {
  const [oldPassword, setoldPassword] = useState('')
  const [newPassword, setnewPassword] = useState('')
  const [confirmPassword, setconfirmPassword] = useState('')
  
  
  const role = (props.role + '').toUpperCase()

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)
  const navigate = useNavigate()
  let data = JSON.parse(sessionStorage.getItem('userInfo'))
  const token = data.jwt
  const editPassword = () => {
   
    if (Validatepassword()) {
     
      // console.log(data.password)
      // console.log(data.id)

      // if (data.password === oldPassword) {
      //   axios
      //     .post("http://localhost:8090/examPortal/user/login", {
      //       // id,
      //       newPassword,
      //     })
      //     .then((response) => {
      //       toast.success("password changed successfully")
      //       Navigate("/login-student")
      //     })
      //     .catch((error) => {
      //       toast.error("Failed to Login")
      //       console.log("error")
      //       console.log(error)
      //     })
      let url = ''
    
      let id = -1
    let email =""
      if (role == 'STUDENT') {url = '/student/edit_password'
      id= data.student.id
      email=data.student.email
    }
      else if (role == 'CLIENT') {url = '/client/edit_password'
    id= data.client.id
    email=data.client.email}
      else {url = '/exam_admin/edit_password' 
    id= data.examAdmin.id
    email=data.examAdmin.email}
     
      // if (data.password === oldPassword) {
        axios
          .put(config.serverURL + url + `/${id}`, {
            email,
            oldPassword,
            newPassword
          },
            {headers: { "Authorization": `Bearer ${token}` }}
          )
          .then((response) => {
            toast.success(response.data.message)
            navigate(props.to)
          })
          .catch((error) => {
            toast.error(error.data.message)
            //console.log('error')
            //console.log(error)
          })
      }
    // }
  }
  function Validatepassword() {
    return (
      oldPassword.length > 0 &&
      newPassword.length > 0 &&
      newPassword === confirmPassword
    )
  }
  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
  },[])

  const style = {
    box: {
      borderStyle: 'solid',
      textAlign: 'center',
    },

    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  return (
    <div className='bgstudent'>
      <div ref={wrapperRef} className='vh-100'style={{ overflow: 'auto' }}>
        <div ref={navRef} >
          {role == 'STUDENT' ? (
            <Sidebar data={StudentData} title='Arjuna Exam Portal' />
          ) : role == 'CLIENT' ? (
            <Sidebar data={ClientData} title='Arjuna Exam Portal' />
          ) : (
            <Sidebar data={ExamAdminData} title='Arjuna Exam Portal' />
          )}
        </div>
        <div
          ref={restRef}
          className='d-flex align-items-center justify-content-center pt-5 py-5 '
          style={style.vh}>
          {/* <div className='st_title'>Change Password</div> */}
          <div className='container px-2 '>
            <div className='d-flex align-items-center justify-content-center pt-2 py-1'></div>
            <div className='col col-11 col-lg-6 mx-auto boxShadow rounded-3 p-3 translogin'>
              <div className='row ps-3 ps-sm-5 pe-3 pe-sm-5 pt-2 '></div>
              <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-1'>
                Change Password
              </div>
              <hr />
              <div className='mb-3'>
                <label for='oldpassword' class='form-label'>
                  Old Password
                </label>
                <input
                  type='password'
                  class='form-control'
                  id='oldpassword'
                  onChange={(e) => setoldPassword(e.target.value)}
                />
              </div>
              <div class='mb-3'>
                <label for='newpassword' class='form-label'>
                  New Password
                </label>
                <input
                  type='password'
                  class='form-control'
                  id='newpassword'
                  placeholder='8-10 characters'
                  onChange={(e) => setnewPassword(e.target.value)}
                />
              </div>
              <div id='passwordHelpBlock' class='form-text'></div>
              <div class='mb-3'>
                <label for='confirmpassword' class='form-label'>
                  Confirm Password
                </label>
                <input
                  type='password'
                  class='form-control'
                  id='confirmpassword'
                  onChange={(e) => setconfirmPassword(e.target.value)}
                />
              </div>

              <button
                type='submit'
                class='col col-12 btn btn-primary'
                onClick={editPassword}>
                Submit
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default EditPassword
