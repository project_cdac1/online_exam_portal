// import axios from "axios"
// import { useEffect, useRef, useState } from "react"
// import { Link, Navigate, useNavigate } from "react-router-dom"
// import { toast } from "react-toastify"
// import config from "../../config"
// import { ExamAdminData } from "../../Data/sidebarData"
// import RegisterForm from "../Client/RegisterForm"
// import PostForm from "./postForm"

import axios from 'axios'
import { useState } from 'react'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../../config'
import PostForm from './postForm'

const MarkingScheme = () => {
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )
  // const [wrapperHeight, setWrapperHeight] = useState(0)
  // const [navHeight, setNavHeight] = useState(0)
  // const [restHeight, setRestHeight] = useState(0)
  // const wrapperRef = useRef(null)
  // const navRef = useRef(null)
  // const restRef = useRef(null)
  // const title = "Institute Name"
  useEffect(() => {
    // setWrapperHeight(wrapperRef.current.clientHeight)
    // setNavHeight(navRef.current.clientHeight)
    // setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    console.log('useeffect')
    getExamList()
  }, [])

  const sessionData = JSON.parse(sessionStorage.getItem('userInfo')).examAdmin
  const getExamList = () => {
    axios
      .get(config.serverURL + '/exam/get_Exam_List' + `/${sessionData.id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
       // console.log(response.data)
        setExamList(response.data)
        //console.log("hello")
        response.data.map((d) => {
          let id = d.id
          let name = d.examName
          examIdList.push({ id, name })
          // console.log('exam id' + d.examName)
        })
        setTest(examIdList)
      })
  }
  const navigate = useNavigate()
  const style = {
    box: {
      borderStyle: 'solid',
      textAlign: 'center',
    },

    vh: {
      //minHeight: restHeight * 0.99,
    },
  }
  const [test, setTest] = useState([])
  const [examId, setExamId] = useState('')
  const [gradeA, setGradeA] = useState('')
  const [gradeB, setGradeB] = useState('')
  const [gradeC, setGradeC] = useState('')
  const [gradeF, setGradeF] = useState('')
  const [examList, setExamList] = useState([])
  let examIdList = []

  const data = [
    {
      label: 'Select Exam',
      func: setExamId,
      isDropDown: true,
      listData: test,
    },

    {
      label: 'Percentage for A Grade',
      func: setGradeA,
      type: 'number',
    },
    {
      label: 'Percentage for B Grade',
      func: setGradeB,
      type: 'number',
    },
    {
      label: 'Percentage for C Grade',
      func: setGradeC,
      type: 'number',
    },
    {
      label: 'Percentage for Pass Grade',
      func: setGradeF,
      type: 'number',
    },
  ]
  const Submit = () => {
    axios
      .post(
        config.serverURL + '/exam/create_Grsch',
        {
          gradeA,
          gradeB,
          gradeC,
          gradeF,
          examId,
        },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((response) => {
        toast.success(response.data.message)

        console.log(response.data)
        navigate('/exam-admin/home')
      })
      .catch((error) => {
        toast.error(error.data.message)
      })
  }
  const submitButton = {
    title: 'Register',

    color: 'btn-primary',
    func: Submit,
  }

  return (
    <div className='bgexamadmin'>
    <PostForm
      data={data}
      submitButton={submitButton}
      title='Add Grading Scheme'
    />
    </div>
  )
}

export default MarkingScheme
