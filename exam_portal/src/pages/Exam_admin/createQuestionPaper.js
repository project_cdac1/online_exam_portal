import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import config from '../../config'
import { ExamAdminData } from '../../Data/sidebarData'

const CreateQuestionPaper = () => {
  const test = [
    { no: '', question: '', optA: '', optB: '', optC: '', optD: '', ans: '' },
  ]
  let tushar = []
  const body = []
  const { state } = useLocation()
  console.log(state)
  const noOfQues = state
  const [list, setlList] = useState([])
  const selectedExamId = JSON.parse(sessionStorage.getItem('selectedExamId'))
  const ques = []
  const optA = []
  const optB = []
  const optC = []
  const optD = []
  const ans = []
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )
  const navigate = useNavigate()
  for (let i = 0; i < noOfQues; i++) {
    ques.push('')
    optA.push('')
    optB.push('')
    optC.push('')
    optD.push('')
    ans.push('')
  }

  const func = () => {
    console.log(ques)
    console.log(optA)
    console.log(optB)
    console.log(optC)
    console.log(optD)
    console.log(ans)
  }
  const submit = () => {
    for (let i = 0; i < noOfQues; i++) {
      if (
        ques[i].length === 0 ||
        optA[i].length === 0 ||
        optB[i].length === 0 ||
        optC[i].length === 0 ||
        optD[i].length === 0 ||
        ans[i].length === 0
      ) {
        toast.warning('Please fill all fields')
        return
      }
    }
    const body = []

    for (let i = 0; i < noOfQues; i++) {
      body.push({
        quesNo: i + 1,
        question: ques[i],
        optA: optA[i],
        optB: optB[i],
        optC: optC[i],
        optD: optD[i],
        ans: ans[i],
      })
    }
    const id = JSON.parse(sessionStorage.getItem('selectedExamId'))
    console.log(id)
    axios
      .post(config.serverURL + `/question/add_quest_list/${id}`, body, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        toast('Question Paper Submitted')
        navigate('/exam-admin/home')
      })
      .catch((error) => {
        // toast.error('error')
        toast.error(error.response.data.message)
      })
   // console.log(body)
  }
  let items = []
  for (let i = 0; i < noOfQues; i++) {
    items.push(
      <div className='col col-11 col-lg-8 boxShadow rounded-3 p-3 mx-auto m-5 translogin'>
        <div className='row ps-3 ps-sm-5 pe-3 pe-sm-5 pt-2 pb-2'>
          <div className='mb-2'>
            <b>Question No : {i + 1}</b>
          </div>
          <div className='mb-3 col col-12'>
            <label>Enter Question</label>
            <textarea
              id='QuestionText'
              className='form-control '
              type='text '
              onChange={(e) => {
                ques[i] = e.target.value
              }}
            />
          </div>
          <hr />
          <div className='mb-3 col col-12 col-lg-6'>
            <label>Option A</label>
            <textarea
              id='optA'
              className='form-control'
              type='text'
              onChange={(e) => {
                optA[i] = e.target.value
              }}
            />
          </div>
          <div className='mb-3 col col-12 col-lg-6'>
            <label>Option B</label>
            <textarea
              id='optB'
              className='form-control'
              type='text'
              onChange={(e) => {
                optB[i] = e.target.value
              }}
            />
          </div>
          <div className='mb-3 col col-12 col-lg-6'>
            <label>Option C</label>
            <textarea
              id='optC'
              className='form-control'
              type='text'
              onChange={(e) => {
                optC[i] = e.target.value
              }}
            />
          </div>
          <div className='mb-3 col col-12 col-lg-6'>
            <label>Option D</label>
            <textarea
              id='optD'
              className='form-control'
              type='text'
              onChange={(e) => {
                optD[i] = e.target.value
              }}
            />
          </div>
          <div className='mb-3 col col-12 col-lg-12'>
            {/* <label className='mb-3 col col-12 col-lg-3'>
                  Select correct option{' '}
                </label>
                <input id='ans' className='mb-3 ' min='1' max='4' type='Number' /> */}
            <div className='mb-3 col col-12'>
              <label>Select Correct Option</label>
              <div className='row'>
                <div className='col col-3 col-md-2'>
                  <label>
                    <input
                      type='radio'
                      name={i}
                      value='optA'
                      onChange={(e) => {
                        ans[i] = e.target.value
                      }}
                    />
                    <span> optA</span>
                  </label>
                </div>
                <div className='col col-3 col-md-2'>
                  <label>
                    <input
                      type='radio'
                      name={i}
                      value='optB'
                      onChange={(e) => {
                        ans[i] = e.target.value
                      }}
                      //   onChange={(e) => d.func(e.target.value)}
                    />
                    <span> optB</span>
                  </label>
                </div>
                <div className='col col-3 col-md-2'>
                  <label>
                    <input
                      type='radio'
                      name={i}
                      value='optC'
                      onChange={(e) => {
                        ans[i] = e.target.value
                      }}
                      //   onChange={(e) => d.func(e.target.value)}
                    />
                    <span> optC</span>
                  </label>
                </div>
                <div className='col col-3 col-md-2'>
                  <label>
                    <input
                      type='radio'
                      name={i}
                      value='optD'
                      onChange={(e) => {
                        ans[i] = e.target.value
                      }}
                      //   onChange={(e) => d.func(e.target.value)}
                    />
                    <span> optD</span>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  //   }, [])

  return (
    <div className='vh-100 bgexamadmin' style={{ overflow: 'auto' }}>
      <Sidebar data={ExamAdminData} title='Arjuna Exam Portal' />
      <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3 m-4'>
        Create Question Paper
      </div>
      {items}
      <div className='col col-6 mx-auto m-5'>
        <button
          className='btn btn-lg btn-primary col col-12 mx-auto'
          onClick={submit}>
          Submit
        </button>
      </div>
    </div>
  )
}

export default CreateQuestionPaper
