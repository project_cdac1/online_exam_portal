import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import config from '../../config'
import { ExamAdminData } from '../../Data/sidebarData'

const AddQuestion = () => {
  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    //console.log(selectedQuestion)
    //getQuestion()
  })
  const submit = () => {
    // check if user has really entered any value
    // if (examName.length === 0) {
    //   toast.error("please enter examName")
    // } else if (noOfQues.length === 0) {
    //   toast.error("please enter noOfQues")
    // } else if (marksPerQues.length === 0) {
    //   toast.error("please enter marksPerQues")
    // } else if (negMarksPerQues.length === 0) {
    //   toast.error("please enter negMarksPerQues")
    // } else if (scheduledTime.length === 0) {
    //   toast.error("please enter scheduledTime")
    // } else if (scheduledDate.length === 0) {
    //   toast.error("please enter ExamDate")
    // } else {
    // make the API call to check if user exists
    const id = JSON.parse(sessionStorage.getItem('selectedQuestion')).id
    const quesNo = JSON.parse(sessionStorage.getItem('selectedQuestion')).quesNo
    console.log(id)
    console.log(quesNo)
    console.log(token)
    axios
      .put(
        config.serverURL + '/question/update_quest',
        {
          id: id,
          quesNo: quesNo,
          question: question,
          optA: optA,
          optB: optB,
          optC: optC,
          optD: optD,
          answer: answer,
          // id,
          // quesNo,
          // question,
          // optA,
          // optB,
          // optC,
          // optD,
          // answer,
        },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((response) => {
        const result = JSON.stringify(response.data)
        //console.log(result)
        //sessionStorage.setItem("mydata", result)
        toast.success('Exam Created Successfully')
        //console.log(JSON.parse(sessionStorage.getItem("mydata")))
        //navigate("/client-home")
      })
      .catch((error) => {
         toast.error(error.response.data.message)
        //console.log(error)
        //console.log(error.data.message)
      })
  }

  const style = {
    box: {
      borderStyle: 'solid',
      textAlign: 'center',
    },

    vh: {
      minHeight: restHeight * 0.99,
    },
  }
  JSON.parse(sessionStorage.getItem('selectedQuestion'))

  // "examId": 1,
  // "quesNo": 3,
  // "question": "Currency of Italy is?",
  // "optA": "Ruppee",
  // "optB": "Yen",
  // "optC": "Doller",
  // "optD": "Euro",
  // "answer": "optD"
 // console.log(JSON.parse(sessionStorage.getItem('selectedQuestion')).question)
 // console.log()
  const [question, setQuestion] = useState(
    JSON.parse(sessionStorage.getItem('selectedQuestion')).question
  )
  const [optA, setoptA] = useState(
    JSON.parse(sessionStorage.getItem('selectedQuestion')).optA
  )
  const [optB, setoptB] = useState(
    JSON.parse(sessionStorage.getItem('selectedQuestion')).optB
  )
  const [optC, setoptC] = useState(
    JSON.parse(sessionStorage.getItem('selectedQuestion')).optC
  )
  const [optD, setoptD] = useState(
    JSON.parse(sessionStorage.getItem('selectedQuestion')).optD
  )
  const [answer, setAnswer] = useState(
    JSON.parse(sessionStorage.getItem('selectedQuestion')).answer
  )

  return (
    <div ref={wrapperRef} className='vh-100 bgexamadmin' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={ExamAdminData} title='Arjuna Exam Portal ' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 p-3 translogin'>
          <div className='row ps-3 ps-sm-5 pe-3 pe-sm-5 pt-2 pb-2'>
            <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
              Update Question
            </div>
            <hr />
            <div className='mb-3 col col-12'>
              <label>Enter Question</label>
              <textarea
                id='question'
                className='form-control '
                type='text '
                value={question}
                onChange={(e) => {
                  setQuestion(e.target.value)
                }}
              />
            </div>
            <hr />
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Option A</label>
              <textarea
                id='optA'
                className='form-control'
                type='text'
                value={optA}
                onChange={(e) => {
                  setoptA(e.target.value)
                }}
              />
            </div>
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Option B</label>
              <textarea
                id='optB'
                className='form-control'
                type='text'
                value={optB}
                onChange={(e) => {
                  setoptB(e.target.value)
                }}
              />
            </div>
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Option C</label>
              <textarea
                id='optC'
                className='form-control'
                type='text'
                value={optC}
                onChange={(e) => {
                  setoptC(e.target.value)
                }}
              />
            </div>
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Option D</label>
              <textarea
                id='optD'
                className='form-control'
                type='text'
                value={optD}
                onChange={(e) => {
                  setoptD(e.target.value)
                }}
              />
            </div>
            <div className='mb-3 col col-12 col-lg-12'>
              <label>Answer</label>
              <textarea
                id='answer'
                className='form-control'
                type='text'
                value={answer}
                onChange={(e) => {
                  setAnswer(e.target.value)
                }}
              />
            </div>
            <button
              className='btn btn-primary col col-6 mx-auto'
              onClick={submit}>
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AddQuestion
