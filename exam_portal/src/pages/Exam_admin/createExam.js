import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import { ExamAdminData } from '../../Data/sidebarData'
import config from '../../config'
const CreateExam = () => {
  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
  })

  const style = {
    box: {
      borderStyle: 'solid',
      textAlign: 'center',
    },

    vh: {
      minHeight: restHeight * 0.99,
    },
  }
  const sessionData = JSON.parse(sessionStorage.getItem('userInfo'))
  const examAdminID = JSON.parse(sessionStorage.getItem('userInfo')).examAdmin.id
  const examStatus = 'false'
  const [examName, setexamName] = useState('')
  const [noOfQues, setnoOfQues] = useState('')
  const [marksPerQues, setmarksPerQues] = useState('')
  const [negMarksPerQues, setnegMarksPerQues] = useState('')
  const [scheduledTime, setscheduledTime] = useState('')
  const [scheduledDate, setscheduledDate] = useState('')
  const [examDuration, setExamDuration] = useState('')
  const [resultDate, seResultDate] = useState('')
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )

  const navigate = useNavigate()
  // const [examAdminID, setexamAdminID] = useState()
  // const [examStatus, setexamStatus] = useState("false")

  // const [ExamDate, setExamDate] = useState("")
  const submit = () => {
    // check if user has really entered any value
    if (examName.length === 0) {
      toast.error('please enter examName')
    } else if (noOfQues.length === 0) {
      toast.error('please enter noOfQues')
    } else if (marksPerQues.length === 0) {
      toast.error('please enter marksPerQues')
    } else if (negMarksPerQues.length === 0) {
      toast.error('please enter negMarksPerQues')
    } else if (scheduledTime.length === 0) {
      toast.error('please enter scheduledTime')
    } else if (scheduledDate.length === 0) {
      toast.error('please enter ExamDate')
    } else {
      // make the API call to check if user exists
      axios
        .post(config.serverURL + '/exam/create_exam',
          {
            examName,
            noOfQues,
            marksPerQues,
            negMarksPerQues,
            scheduledTime,
            scheduledDate,
            examAdminID,
            examDuration,
            resultDate,
          },
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((response) => {
          const result = JSON.stringify(response.data)
          //console.log(result)
          sessionStorage.setItem('mydata', result)
          toast.success('Exam Created Successfully')
          //console.log(JSON.parse(sessionStorage.getItem('mydata')))
          navigate('/exam-admin/home')
        })
        .catch((error) => {
          toast.error(error.response.data.message)
         // console.log('error')
          //console.log(error)
        })
    }
  }

  return (
    <div ref={wrapperRef} className='vh-100 bgexamadmin'style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={ExamAdminData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 p-3 translogin'>
          <div className='row ps-3 ps-sm-5 pe-3 pe-sm-5 pt-2 pb-2'>
            <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
              Create Exam
            </div>
            <hr />
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Exam Name</label>
              <input
                id='examName'
                className='form-control'
                type='text'
                onChange={(e) => {
                  setexamName(e.target.value)
                }}
              />
            </div>

            <div className='mb-3 col col-12 col-lg-6'>
              <label>No of Questions</label>
              <input
                id='noOfQues'
                className='form-control'
                type='number'
                onChange={(e) => {
                  setnoOfQues(e.target.value)
                }}
              />
            </div>

            <div className='mb-3 col col-12 col-lg-6'>
              <label>Marks Per Question</label>
              <input
                id='marksPerQues'
                className='form-control'
                type='number'
                onChange={(e) => {
                  setmarksPerQues(e.target.value)
                }}
              />
            </div>

            <div className='mb-3 col col-12 col-lg-6'>
              <label> Negative Marks Per Question</label>
              <input
                id='negMarksPerQues'
                className='form-control'
                type='Number'
                onChange={(e) => {
                  setnegMarksPerQues(e.target.value)
                }}></input>
            </div>
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Exam Duration (in Minute)</label>
              <input
                id='examName'
                className='form-control'
                type='number'
                onChange={(e) => {
                  setExamDuration(e.target.value)
                }}
              />
            </div>
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Schedule Time</label>
              <input
                id='scheduledTime'
                className='form-control'
                type='time'
                onChange={(e) => {
                  setscheduledTime(e.target.value)
                }}
              />
            </div>
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Schedule Date</label>
              <input
                id='ExamDate'
                className='form-control'
                type='Date'
                onChange={(e) => {
                  setscheduledDate(e.target.value)
                }}
              />
            </div>
            <div className='mb-3 col col-12 col-lg-6'>
              <label>Result Date</label>
              <input
                id='resultDate'
                className='form-control'
                type='date'
                onChange={(e) => {
                  seResultDate(e.target.value)
                }}
              />
            </div>
            <div className='mb-3 col col-12 col-lg-12'></div>
            <button
              onClick={submit}
              className='col col-4  col-lg-4 btn btn-primary   mx-auto '>
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CreateExam
