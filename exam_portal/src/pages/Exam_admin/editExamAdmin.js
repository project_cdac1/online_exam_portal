import axios from 'axios'
import { useEffect } from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import RegisterForm from '../../components/Form/RegisterForm'
import config from '../../config'

const EditExamAdmin = () => {
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )

  const getPincodeList = () => {
    axios
      .get(config.serverURL + '/address/get_pincode_list', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        const result = []
        result.push(...response.data)
        result.splice(result.indexOf(examAdmin.address.pinCode), 1)
        setPinCodeList(result)
        // pinCodeList = result
        // if (result['status'] === 'error') {
        //   toast.error('No List Found')
        // } else {
        // }
      })
  }
  // var subscriptionList
  // var pinCodeList

  const examAdmin = JSON.parse(sessionStorage.getItem('userInfo')).examAdmin
  const [name, setName] = useState(examAdmin.name)
  const [pinCode, setPinCode] = useState(examAdmin.address.pinCode)
  const [pinCodeList, setPinCodeList] = useState([])
  const [email, setEmail] = useState(examAdmin.email)
  const [mobile, setMobile] = useState(examAdmin.mobile)
  const [addressLine1, setAddress1] = useState(examAdmin.addressLine1)
  const [addressLine2, setAddress2] = useState(examAdmin.addressLine2)
  const [department, setDepartment] = useState(examAdmin.department)
  const [dob, setDob] = useState(examAdmin.dob)
  const [gender, setGender] = useState(examAdmin.gender)
 

  let address = examAdmin.address
  const navigate = useNavigate()

  const register = () => {
    if (name.length === 0) {
      toast.error('please enter org name')
    } else if (pinCode.length === 0) {
      toast.error('please enter pincode')
    } else if (email.length === 0) {
      toast.error('please enter email')
    } else if (mobile.length === 0) {
      toast.error('please enter phone number')
    } else if (addressLine1.length === 0) {
      toast.error('please enter address')
    } else if (department.length === 0) {
      toast.error('please enter department')
    } else {
      const body = {
        id: examAdmin.id,
        name,
        pinCode,
        email,
        mobile,
        addressLine1,
        addressLine2,
        department,
        dob,
        gender,
      }
      axios
        .post(config.serverURL + '/exam_admin/update_exam_admin', body, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((response) => {
           const result = JSON.stringify(response.data)
            sessionStorage.setItem('userInfo', result)
            toast.success('Information Updated successfully')
            navigate('/login-examadmin')
          }
        )
        .catch((error) => {
          //console.log(examAdmin)
          // console.log(examAdmin.id)
          // console.log(error)
          toast.error('Failed to update')
        })
    }
  }

  useEffect(() => {
    getPincodeList()
    // client = JSON.parse(sessionStorage.getItem('userInfo'))
  }, [])

  const data = [
    {
      label: 'Name',
      func: setName,
      isFullWidth: true,
      value: name,
    },
    {
      label: 'AddressLine1',
      func: setAddress1,
      value: addressLine1,
    },
    {
      label: 'Address2',
      func: setAddress2,
      value: addressLine2,
    },
    {
      label: 'PinCode',
      func: setPinCode,
      isDropDown: true,
      listData: pinCodeList,
      // value: address.pinCode,
      selectedValue: address.pinCode,
    },
    {
      label: 'Email',
      func: setEmail,
      value: email,
    },
    {
      label: 'BirthDate',
      func: setDob,
      value: dob,
      type: 'date',
    },
    {
      label: 'Gender',
      func: setGender,
      isRadio: true,
      list: ['MALE', 'FEMALE', 'OTHER'],
    },
    {
      label: 'Contact Number',
      func: setMobile,
      value: mobile,
    },
    {
      label: 'Department',
      func: setDepartment,
      value: department,
    },
  ]

  const submitButton = {
    title: 'Register',
    color: 'btn-success',
    func: register,
  }

  return (
    <div className='bgexamadmin'>
    <RegisterForm
      data={data}
      submitButton={submitButton}
      title='Edit Profile'
      examAdmin={true}
    />
    </div>
  )
}

export default EditExamAdmin
