// import axios from "axios"
// import React, { useRef } from "react"
// import { useEffect } from "react"
// import { useState } from "react"
// import { useNavigate } from "react-router-dom"
// import Sidebar from "../../components/sidebar/sidebar"
// import config from "../../config"
// import { ExamAdminData } from "../../Data/sidebarData"

import axios from 'axios'
import { useRef } from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import config from '../../config'
import { ExamAdminData } from '../../Data/sidebarData'

const UpdateQuestion = () => {
  const sessionData = JSON.parse(sessionStorage.getItem('userInfo'))
  const selectedExamId = JSON.parse(sessionStorage.getItem('selectedExamId'))
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )

  const getQuestionList = () => {
    axios
      .get(config.serverURL + '/exam/get_questions' + `/${selectedExamId}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        //console.log(response.data)
        setQuestionList(response.data)
      })
      .catch((error) => {
        toast('error to fetch exam list')
      })
  }
  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)
  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }
  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    //console.log('useeffect')
    getQuestionList()
  }, [])
  const selectedQuestionID = (selectedQuestion) => {
    sessionStorage.setItem('selectedQuestion', JSON.stringify(selectedQuestion))

    //console.log(selectedQuestion)

    navigate('/Add-Question')
  }

  const [questionList, setQuestionList] = useState([])

  const navigate = useNavigate()
  return (
    <div ref={wrapperRef} className='vh-100 bgexamadmin' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={ExamAdminData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 px-5 py-3 text-center table-responsive translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
            Select Question
          </div>
          <hr />
          <table className='table table-hover table-secondary'>
            <thead>
              <tr>
                <th>Qusetion No</th>
                <th>Question</th>
                <th>Option A</th>
                <th>Option B</th>
                <th>Option C</th>
                <th>Option D</th>
                <th>Answer</th>

                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {questionList.map((l) => {
                return (
                  <tr>
                    <td>{l.quesNo}</td>
                    <td>{l.question}</td>
                    <td>{l.optA}</td>
                    <td>{l.optB}</td>
                    <td>{l.optC}</td>
                    <td>{l.optD}</td>
                    <td>{l.answer}</td>
                    <td>
                      <button
                        onClick={() => selectedQuestionID(l)}
                        className='btn btn-warning w-100'>
                        Update
                      </button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default UpdateQuestion
