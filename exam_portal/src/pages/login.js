// import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
// import config from '../../config'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'
import Input from '../components/input'
import { useEffect, useRef, useState } from 'react'
import Navbar from '../components/navbar'
import config from '../config'

const Signin = (props) => {
  // get user inputs
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const role = (props.role + '').toUpperCase()
  // get the navigate function reference
  const navigate = useNavigate()
  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
  })

  const login = () => {
    // check if user has really entered any value
    if (email.length === 0) {
      toast.error('please enter email')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else {
      // make the API call to check if user exists
      axios
        .post(config.serverURL + '/user/login', {
          email,
          password,
          role,
        })
        .then((response) => {
          const result = JSON.stringify(response.data)
          // console.log(result)
          sessionStorage.setItem('userInfo', result)
         
          toast.success('Login Successfull')
         // console.log(JSON.parse(sessionStorage.getItem('userInfo')))
          navigate(props.to)
        })
        .catch((error) => {
         // const result = error.response.data.message
          // console.log(result)
         toast.error("failed to login")
          // console.log('error')
        })
    }
  }

  return (
    <div ref={wrapperRef} className='min-vh-100 bgimage'>
      <div ref={navRef}>
        <Navbar />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-4 boxShadow rounded-3 px-5 py-3 translogin'>
          <div className='row'>
            <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3 border-bottom border-success border-3 mb-3'>
              {props.role} Login
            </div>
            <Input
              title='Email'
              onChange={(e) => {
                setEmail(e.target.value)
              }}
            />
            <Input
              title='Password'
              type='password'
              onChange={(e) => {
                setPassword(e.target.value)
              }}
            />
            <Link
              to='/forgot-password'
              state={{ from: props.role }}
              className='col col-12 text-primary fw-bold text-center'>
              Forgot Password
            </Link>
            <button
              className='btn btn-success col-6 mx-auto mt-3'
              onClick={login}>
              <b>LogIn</b>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Signin
