// import { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import axios from 'axios'
// import config from '../../config'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'
import Input from '../components/input'
import { useEffect, useRef, useState } from 'react'
import Navbar from '../components/navbar'
import config from '../config'

const ForgotPassword = () => {
  // get user inputs
  const [email, setEmail] = useState('')
  const [code, setCode] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const location = useLocation()
  const { from } = location.state
  console.log(from)
  // get the navigate function reference
  const navigate = useNavigate()
  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
  })

  const send = () => {
    // check if user has really entered any value
    if (email.length === 0) {
      toast.warning('please enter email')
    } else {
      // make the API call to check if user exists
      const body = {
        email,
        role: from,
      }
      console.log(body)
      axios
        .post(config.serverURL + '/forgot_password', body)
        .then((response) => {
          //console.log(response)
          toast.success(response.data.message)
        })
        .catch((error) => {
          // const result = error.response.data.message
          // console.log(result)
          toast.error('error')
          // console.log('error')
        })
    }
  }

  const submit = () => {
    axios
      .post(config.serverURL + '/recover_password', {
        email,
        role: from,
        oldPassword: code,
        newPassword: password,
      })
      .then((response) => {
        toast.success(response.data.message)
        navigate('/')
      })
      .catch((error) => {
        toast(error.response.data.message)
      })
  }
  return (
    <div ref={wrapperRef} className='vh-100 bgimage' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Navbar />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-md-8 col-lg-6 boxShadow rounded-3 px-5 py-3 translogin'>
          <div className='row'>
            <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3 border-bottom border-success border-3 mb-3'>
              Forgot Password
            </div>
            <Input
              title='Email'
              onChange={(e) => {
                setEmail(e.target.value)
              }}
            />
            <div className='mx-auto col col-9 mb-3'>
              <button className='btn btn-primary col col-3 me-4' onClick={send}>
                Send
              </button>
              <span className='fst-italic text-dark'>
                Enter Email and hit Send to get Recovery Code
              </span>
            </div>
            <div className='border-bottom border-dark border-2 mb-3'></div>
            <Input
              title='Recovery Code'
              onChange={(e) => {
                setCode(e.target.value)
              }}
            />
            <Input
              title='New Password'
              onChange={(e) => {
                setPassword(e.target.value)
              }}
            />
            <Input
              title='Confirm New Password'
              onChange={(e) => {
                setConfirmPassword(e.target.value)
              }}
            />
            <button
              className='btn btn-success col col-6 mx-auto'
              onClick={submit}>
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ForgotPassword
