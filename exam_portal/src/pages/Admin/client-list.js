import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import config from '../../config'
import { AdminData, ClientData, StudentData } from '../../Data/sidebarData'

const ClientList = () => {
  const [list, setList] = useState([])
  const admin=JSON.parse(sessionStorage.getItem('userInfo'))
  const token = admin.jwt
  const navigate = useNavigate()

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  const getList = () => {
    // console.log('test')
    axios
      .get(config.serverURL + '/admin/get_client_details',{headers: { "Authorization": `Bearer ${token}` }})
      .then((response) => {
        const result = response.data
        setList(result)
        //console.log(result)
      })
      .catch((response) => {
        toast.error('Failed to load Client details')
      })
  }
  function toggleClientStatus(id) {
    axios
      .put(config.serverURL + '/client/toggle_client_acc_status' + `/${id}`,{},{headers: { "Authorization": `Bearer ${token}` }})
      .then((response) => {
        // const data = JSON.parse(sessionStorage.getItem('userInfo'))
        getList()
        toast.success(response.data.message)
      })
      .catch((error) => {
        toast.error('Failed to load exam details')
      })
  }

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    getList()
  }, [])

  return (
    <div ref={wrapperRef} className='vh-100 bgadmin' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={AdminData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 px-5 py-3 text-center table-responsive translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
            Client Statistics
          </div>
          <hr />
          <table className='table table-hover table-secondary'>
            <thead>
              <tr>
                <th>Client Name</th>
                <th>Client Email</th>
                <th>Student Count</th>
                <th>Exam Admin Count</th>
                <th>Exam Count</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {list.map((l) => {
                return (
                  <tr>
                    <td>{l.name}</td>
                    <td>{l.email}</td>
                    <td>{l.studentCount}</td>
                    <td>{l.examAdminCount}</td>
                    <td>{l.examCount}</td>

                    <td>
                      {l.accStatus ? (
                        <div
                          onClick={() => toggleClientStatus(l.id)}
                          className='btn btn-danger w-100'>
                          Deactivate
                        </div>
                      ) : (
                        <button
                          onClick={() => toggleClientStatus(l.id)}
                          className='btn btn-success w-100'>
                          Activate
                        </button>
                      )}
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default ClientList
