import axios from 'axios'
import { useEffect } from 'react'
import { useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import RegisterForm from '../../components/Form/RegisterForm'
import config from '../../config'
import StFormBox from '../../components/Form/st-formbox'

const EditSubscription = () => {
  // var subscriptionList
  // var pinCodeList
  const admin=JSON.parse(sessionStorage.getItem('userInfo'))
  const token = admin.jwt
  //   const student=JSON.parse(sessionStorage.getItem('userInfo'))
  //   let address =student.address
  const { state } = useLocation()
  const [planName, setPlanName] = useState()
  const [noOfExams, setNoOfExams] = useState()
  const [price, setPrice] = useState()

  const navigate = useNavigate()

  const editSubscription = () => {
    // console.log({ gender })
    // console.log({ dob })
    const body = {
      id: state,
      planName: planName,
      noOfExams: noOfExams,
      price: price,
    }
    axios
      .put(config.serverURL + '/subscription/edit_subscription', body,{headers: { "Authorization": `Bearer ${token}` }})
      .then((response) => {
        //const result = response.data
        const result = JSON.stringify(response.data)
       

        toast.success("Subscription details updated")
        navigate('/select-subscription')
      })
      .catch((error) => {
        toast.error(error.data.message)
      })
  }

  const getSubscription = (id) => {
    axios
      .get(config.serverURL + `/subscription/get_sub_by_id/${id}`,{headers: { "Authorization": `Bearer ${token}` }})
      .then((response) => {
        var result = response.data
        setPlanName(result.planName)
        setNoOfExams(result.noOfExams)
        setPrice(result.price)
        // pinCodeList = result
        // if (result['status'] === 'error') {
        //   toast.error('No List Found')
        // } else {
        // }
      })
      .catch((error) => {
        //console.log(error.data)
        toast.error("no details available")
      })
  }

  useEffect(() => {
    // const ids = i.id
    // console.log(i.id)
    //setStudentId(student.id)
    getSubscription(state)
  }, [])

  const data = [
    {
      label: 'Plan Name',
      func: setPlanName,
      isFullWidth: true,
      value: planName,
    },
    {
      label: 'No of Exams',
      func: setNoOfExams,
      value: noOfExams,
    },
    {
      label: 'Price',
      func: setPrice,
      value: price,
    },
  ]

  const submitButton = {
    title: 'Edit Plan',
    color: 'btn-success',
    func: editSubscription,
  }

  return (
    <div className='bgadmin'>
    <StFormBox
    adminSideBar= {true}
      data={data}
      submitButton={submitButton}
      title='Edit-Subscription'
    />
    </div>
  )
}

export default EditSubscription
