import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import config from '../../config'
import { AdminData, ClientData, StudentData } from '../../Data/sidebarData'

const SelectSubscription = () => {
  const [list, setList] = useState([])

  const navigate = useNavigate()

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  const getList = () => {
    // console.log('test')
    axios
      .get(config.serverURL + '/subscription/get_sub_details')
      .then((response) => {
        const result = response.data
        setList(result)
        //console.log(result)
      })
      .catch((response) => {
        toast.error('Failed to load Client details')
      })
  }
  const edit = (id) => {
    navigate('/edit-subscription', { state: id })
  }
  //   function toggleClientStatus(id) {
  //     axios
  //       .put(config.serverURL + '/client/toggle_client_acc_status' + `/${id}`, {})
  //       .then((response) => {
  //         // const data = JSON.parse(sessionStorage.getItem('userInfo'))
  //         getList()
  //         toast.success(response.data.message)
  //       })
  //       .catch((error) => {
  //         toast.error('Failed to load exam details')
  //       })
  //   }

  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    getList()
  }, [])

  return (
    <div ref={wrapperRef} className='vh-100 bgadmin' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={AdminData} title='Institute Name' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 px-5 py-3 text-center table-responsive translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
            Select Subscription
          </div>
          <hr />
          <table className='table table-hover table-secondary'>
            <thead>
              <tr>
                <th>Plan Name</th>
                <th>No of Exams </th>
                <th>Price</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {list.map((l) => {
                return (
                  <tr>
                    <td>{l.planName}</td>
                    <td>{l.noOfExams}</td>

                    <td> &#8377;{l.price}</td>
                    <td>
                      <button
                        onClick={() => edit(l.id)}
                        className='btn btn-primary w-100'>
                        Edit
                      </button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default SelectSubscription
