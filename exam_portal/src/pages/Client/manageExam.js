import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import config from '../../config'
import { ClientData } from '../../Data/sidebarData'

const ManageExam = () => {
  const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))
  console.log(userInfo)
  const token = userInfo.jwt
  const [list, setList] = useState([])
  const navigate = useNavigate()
  let data
  function toggleStatus(id) {
   //console.log(token)
    axios
      .put(config.serverURL + '/exam/toggle_exam_status' + `/${id}`,{}, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        const data = JSON.parse(sessionStorage.getItem('userInfo'))
        getList(data.client.id)
        toast.success(response.data.message)
      })
      .catch((error) => {
        toast.error('Failed to load exam details')
      })
  }

  // const deleteHome = (id) => {

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  const getList = (id) => {
    // console.log('test')
    axios
      .get(config.serverURL + '/exam/get_exam_list' + `/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        const result = response.data
        setList(result)
        //console.log(result)
      })
      .catch((response) => {
        toast.error('Failed to load exam details')
      })
  }

  useEffect(() => {
    const data = JSON.parse(sessionStorage.getItem('userInfo'))
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    getList(data.client.id)
  }, [])

  return (
    <div ref={wrapperRef} className='vh-100 bgclient' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={ClientData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 px-5 py-3 text-center table-responsive translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
            Manage Exam
          </div>
          <hr />
          <table className='table table-hover table-secondary'>
            <thead>
              <tr>
                <th>Name</th>
                {/* <th>Exam Admin</th> */}
                <th>Scheduled Date</th>
                <th>Scheduled Time</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {list.map((l) => {
                return (
                  <tr>
                    <td>{l.examName}</td>
                    {/* <td>{l.examAdmin}</td> */}
                    <td>{l.scheduledDate}</td>
                    <td>{l.scheduledTime}</td>
                    <td>
                      {l.examStatus ? (
                        <div
                          onClick={() => toggleStatus(l.id)}
                          className='btn btn-danger w-100'>
                          Stop
                        </div>
                      ) : (
                        <button
                          onClick={() => toggleStatus(l.id)}
                          className='btn btn-success w-100'>
                          Start
                        </button>
                      )}
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default ManageExam
