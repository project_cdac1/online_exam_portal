import Sidebar from "../../components/sidebar/sidebar"
import { ClientData } from "../../Data/sidebarData"
import { useEffect, useRef, useState } from 'react'
const ClientHome = () => {
  const client=JSON.parse(sessionStorage.getItem('userInfo'))

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }
  // const clientId= student.student.clientId
  // const token = student.jwt
  // const getImpDates = ()=>{
  //   axios.get(config.serverURL + '/student/imp_dates' + `/${clientId}`,{headers: { "Authorization": `Bearer ${token}` }})
  //   .then((response) => {
  //    // console.log(response.data)
  //     setSchedule(response.data)
  //   })
  //   .catch((error)=>{
  //     toast.error("list is empty")
  //    // console.log(error)
  //   })
  // }
  useEffect(() => {
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    // getImpDates()
  },[])

  return (
    <div ref={wrapperRef} className='vh-100 bgclient'style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={ClientData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-6 boxShadow rounded-3 px-5 py-3 table-responsive text-left translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
         Personal Details
          </div>
          <hr />
        <div className='row ms-5 fw-bold  fs-5'>
          <div className='col  col-6'>
            Name 
          </div>
          <div className='col col-6 fst-italic text-success'>
            {client.client.name}
          </div>
  
          <div className='col col-6'>
            Email 
          </div>
          <div className='col col-6 fst-italic text-success'>
            {client.client.email}
          </div>
         
          <div className='col  col-6'>
            Address 
          </div>
          <div className='col col-6 fst-italic text-success'>
            {client.client.address.city}
          </div>
          <div className='col col-6'>
            Contact Number 
          </div>
          <div className='col col-6 fst-italic text-success '>
            {client.client.mobile}
          </div>
          <div className='col col-6'>
            Role 
          </div>
          <div className='col col-6 fst-italic text-success '>
            {client.client.role}
          </div>
          
        </div>

         
        </div>
      </div>
    </div>
  )
}

export default ClientHome
