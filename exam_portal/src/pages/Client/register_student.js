import axios from 'axios'
import { useEffect } from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import RegisterForm from '../../components/Form/RegisterForm'
import config from '../../config'

const RegisterStudent = () => {
  // var subscriptionList
  // var pinCodeList

  const [name, setName] = useState('')
  const [pinCode, setPinCode] = useState('')
  const [pinCodeList, setPinCodeList] = useState([])
  const [email, setEmail] = useState('')
  const [mobile, setMobile] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [addressLine1, setAddress1] = useState('')
  const [addressLine2, setAddress2] = useState('')
  const [dob, setDOB] = useState('')
  const [gender, setGender] = useState('')
  const [rollNo, setRollNo] = useState()
  const [clientId, setClientId] = useState('')
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )

  const navigate = useNavigate()

  const register = () => {
    //console.log({ gender })
    //console.log({ dob })

    if (name.length === 0) {
      toast.error('please enter org name')
    } else if (pinCode.length === 0) {
      toast.error('please enter pincode')
    } else if (email.length === 0) {
      toast.error('please enter email')
    } else if (mobile.length === 0) {
      toast.error('please enter phone number')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else if (confirmPassword.length === 0) {
      toast.error('please confirm password')
    } else if (password !== confirmPassword) {
      toast.error('password does not match')
    } else if (addressLine1.length === 0) {
      toast.error('please enter address')
    } else if (dob.length === 0) {
      toast.error('please enter Date of Birth')
    } else if (gender.length === 0) {
      toast.error('please select Gender')
    } else if (rollNo === null) {
      toast.error('please Enter roll No')
    } else {
      /*
      {
    "name": "Sunbeam",
    "pinCode": "415605",
    "email": "user1@gmail.com",
    "mobile": "7517527737",
    "password": "123",
    "addressLine1": "asd",
    "addressLine2": "asd",
    "clientId" : 2,
    "department" : "PG-DAC",
    "dob" : "1999-10-19",
    "gender" : "MALE"
}
      */
      // const token = JSON.parse(sessionStorage.getItem('userInfo')).jwt
      const body = {
        name,
        pinCode,
        email,
        mobile,
        password,
        addressLine1,
        addressLine2,
        clientId,
        dob,
        gender,
        rollNo,
      }
      axios
        .post(config.serverURL + '/student/register', body, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((response) => {
          const result = response.data
          toast.success('new student added')
          navigate(0)
        })
        .catch((response) => {
          toast.error('Failed to add new student')
        })
    }
  }

  const getPincodeList = () => {
    axios
      .get(config.serverURL + '/address/get_pincode_list', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        const result = []
        // const result = response.data
        result.push('')
        result.push(...response.data)
        setPinCodeList(result)
        // setPinCodeList(pinCodeList.push(''))
        // pinCodeList = result
        // if (result['status'] === 'error') {
        //   toast.error('No List Found')
        // } else {
        // }
      })
      .catch((error) => {
        //console.log(error)
        toast.error(" pincode list is empty")
      })
  }

  useEffect(() => {
    const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))
    // const ids = i.id
    // console.log(i.id)
    setClientId(userInfo.client.id)
    getPincodeList()
  }, [])

  const data = [
    {
      label: 'Name',
      func: setName,
      isFullWidth: true,
      pattern:'^[a-z]{5,80}$',
      errorMessage:'Name should contain between 5-80 characters',
      required: true,
    },
    {
      label: 'AddressLine1',
      func: setAddress1,
      pattern:'^[a-z]{5,80}$',
      errorMessage:'Address should be supplied',
      required: true,
    },
    {
      label: 'AddressLine2',
      func: setAddress2,
    },
    {
      label: 'PinCode',
      func: setPinCode,
      isDropDown: true,
      listData: pinCodeList,
      pattern:'^(?!\s*$).+',
      errorMessage:'Please select pincode',
      required: true,
    },
    {
      label: 'Gender',
      func: setGender,
      isRadio: true,
      list: ['MALE', 'FEMALE', 'OTHER'],
    },
    {
      label: 'Date of Birth',
      func: setDOB,
      type: 'date',
    },
    {
      label: 'Email',
      func: setEmail,
      pattern:'^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$',
      errorMessage:'Please enter valid email address',
      required: true,
    },
    {
      label: 'Contact Number',
      func: setMobile,
      type: 'number',
    },
    {
      label: 'Roll No',
      func: setRollNo,
      type: 'number',
      pattern:'^[0-9]{1,10}$',
      errorMessage:'Roll number must be supplied',
      required: true,
    },
    {
      label: 'Password',
      func: setPassword,
      pattern:'^[A-Za-z0-9]{3,10}$',
      errorMessage:'Password should contain 3-10 characters',
      required: true,
    },
    {
      label: 'Confirm Password',
      func: setConfirmPassword,
      type: 'password',
      errorMessage:'Password not matched',
      required: true,
    },
  ]

  const submitButton = {
    title: 'Register',
    color: 'btn-success',
    func: register,
  }

  return (
    <div className='bgclient'>
    <RegisterForm data={data} submitButton={submitButton} title='Add Student' />
    </div>
  )
}

export default RegisterStudent
