import axios from 'axios'
import { useEffect } from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import RegisterForm from '../../components/Form/RegisterForm'
import config from '../../config'

const EditClient = () => {
  const getPincodeList = () => {
    axios
      .get(config.serverURL + '/address/get_pincode_list', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        const result = []
        result.push(...response.data)
        result.splice(result.indexOf(client.address.pinCode), 1)
        setPinCodeList(result)
        // pinCodeList = result
        // if (result['status'] === 'error') {
        //   toast.error('No List Found')
        // } else {
        // }
      })
  }
  // var subscriptionList
  // var pinCodeList

  const client = JSON.parse(sessionStorage.getItem('userInfo')).client
  const [name, setName] = useState(client.name)
  const [pinCode, setPinCode] = useState(client.address.pinCode)
  const [pinCodeList, setPinCodeList] = useState([])
  const [email, setEmail] = useState(client.email)
  const [mobile, setMobile] = useState(client.mobile)
  const [addressLine1, setAddress1] = useState(client.addressLine1)
  const [addressLine2, setAddress2] = useState(client.addressLine2)
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )

  let address = client.address
  const navigate = useNavigate()

  const register = () => {
    if (name.length === 0) {
      toast.error('please enter org name')
    } else if (pinCode.length === 0) {
      toast.error('please enter pincode')
    } else if (email.length === 0) {
      toast.error('please enter email')
    } else if (mobile.length === 0) {
      toast.error('please enter phone number')
    } else if (addressLine1.length === 0) {
      toast.error('please enter address')
    } else {
      const body = {
        id: client.id,
        name,
        pinCode,
        email,
        mobile,
        addressLine1,
        addressLine2,
      }
      axios
        .post(config.serverURL + '/client/update_client', body, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((response) => {
          const result = response.data
          if (result['status'] === '400') {
            toast.error('Failed registered a new user')
          } else {
            const result = JSON.stringify(response.data)
            sessionStorage.setItem('userInfo', result)
            toast.success('Information Updated successfully')
            navigate('/login-client')
          }
        })
        .catch((response) => {
          toast.error('Failed to registered a new user')
        })
    }
  }

  useEffect(() => {
    getPincodeList()
    // client = JSON.parse(sessionStorage.getItem('userInfo'))
  }, [])

  const data = [
    {
      label: 'Organization Name',
      func: setName,
      isFullWidth: true,
      value: name,
    },
    {
      label: 'AddressLine1',
      func: setAddress1,
      value: addressLine1,
    },
    {
      label: 'Address2',
      func: setAddress2,
      value: addressLine2,
    },
    {
      label: 'PinCode',
      func: setPinCode,
      isDropDown: true,
      listData: pinCodeList,
      // value: address.pinCode,
      selectedValue: address.pinCode,
    },
    {
      isEmpty: true,
    },
    {
      label: 'Email',
      func: setEmail,
      value: email,
    },
    {
      label: 'Contact Number',
      func: setMobile,
      value: mobile,
    },
  ]

  const submitButton = {
    title: 'Register',
    color: 'btn-success',
    func: register,
  }

  return (
    <div className='bgclient'>
    <RegisterForm data={data} submitButton={submitButton} title='Edit Client' />
    </div>
  )
}

export default EditClient
