import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Sidebar from '../../components/sidebar/sidebar'
import config from '../../config'
import { ClientData } from '../../Data/sidebarData'

const ManageStudent = () => {
  const [list, setList] = useState([])
  const [token, setToken] = useState(
    JSON.parse(sessionStorage.getItem('userInfo')).jwt
  )
  function toggleStatus(id) {
    axios
      .put(
        config.serverURL + '/student/toggle_student_acc_status' + `/${id}`,
        {},
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((response) => {
        const data = JSON.parse(sessionStorage.getItem('userInfo'))
        getStudentList(data.client.id)
        toast.success(response.data.message)
      })
      .catch((error) => {
        toast.error(error.response.data.message)
      })
  }

  // const deleteHome = (id) => {

  const [wrapperHeight, setWrapperHeight] = useState(0)
  const [navHeight, setNavHeight] = useState(0)
  const [restHeight, setRestHeight] = useState(0)
  const wrapperRef = useRef(null)
  const navRef = useRef(null)
  const restRef = useRef(null)

  const style = {
    vh: {
      minHeight: restHeight * 0.99,
    },
  }

  const getStudentList = (sId) => {
    // console.log('test')
    axios
      .get(config.serverURL + '/student/get_student_list' + `/${sId}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        const result = response.data
        setList(result)
        console.log(result)
      })
      .catch((error) => {
        // console.log(error)
        setList(error.response.data)
        // toast.error(error)
      })
  }

  useEffect(() => {
    const data = JSON.parse(sessionStorage.getItem('userInfo'))
    setWrapperHeight(wrapperRef.current.clientHeight)
    setNavHeight(navRef.current.clientHeight)
    setRestHeight(wrapperRef.current.clientHeight - navRef.current.clientHeight)
    getStudentList(data.client.id)
  }, [])

  return (
    <div ref={wrapperRef} className='vh-100 bgclient' style={{ overflow: 'auto' }}>
      <div ref={navRef}>
        <Sidebar data={ClientData} title='Arjuna Exam Portal' />
      </div>
      <div
        ref={restRef}
        className='d-flex align-items-center justify-content-center pt-5 py-5'
        style={style.vh}>
        <div className='col col-11 col-lg-8 boxShadow rounded-3 px-5 py-3 text-center table-responsive translogin'>
          <div className='fw-bold col col-12 mx-auto fs-1 text-center fst-italic pb-3'>
            Manage Student
          </div>
          <hr />
          <table className='table table-hover table-secondary'>
            <thead>
              <tr>
                <th>Roll No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Birth Date</th>
                <th>Gender</th>
                <th>Account Status</th>
              </tr>
            </thead>
            <tbody>
              {list.map((l) => {
                return (
                  <tr>
                    <td>{l.rollNo}</td>
                    <td>{l.name}</td>
                    <td>{l.email}</td>
                    <td>{l.dob}</td>
                    <td>{l.gender}</td>
                    <td>
                      {l.status ? (
                        <div
                          onClick={() => toggleStatus(l.id)}
                          className='btn btn-danger w-100'>
                          Deactiavte
                        </div>
                      ) : (
                        <button
                          onClick={() => toggleStatus(l.id)}
                          className='btn btn-success w-100'>
                          Activate
                        </button>
                      )}
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default ManageStudent
