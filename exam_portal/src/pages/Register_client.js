import axios from 'axios'
import { useEffect } from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import RegisterForm from '../components/Form/RegisterForm'
import config from '../config'

const RegisterClient = () => {
  // var subscriptionList
  // var pinCodeList

  const [name, setName] = useState('')
  const [pinCode, setPinCode] = useState('')
  const [pinCodeList, setPinCodeList] = useState([])
  const [email, setEmail] = useState('')
  const [mobile, setMobile] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [addressLine1, setAddress1] = useState('')
  const [addressLine2, setAddress2] = useState('')
  const [subscriptionName, setSubscriptionName] = useState()
  const [subscriptionList, setSubscriptionList] = useState([])

  const navigate = useNavigate()

  const register = () => {
    if (name.length === 0) {
      toast.error('please enter org name')
    } else if (pinCode.length === 0) {
      toast.error('please enter pincode')
    } else if (email.length === 0) {
      toast.error('please enter email')
    } else if (mobile.length === 0) {
      toast.error('please enter phone number')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else if (confirmPassword.length === 0) {
      toast.error('please confirm password')
    } else if (password !== confirmPassword) {
      toast.error('password does not match')
    } else if (addressLine1.length === 0) {
      toast.error('please enter address')
    } else if (subscriptionName === null) {
      toast.error('please enter address')
    } else {
      const body = {
        name,
        pinCode,
        email,
        mobile,
        password,
        addressLine1,
        addressLine2,
        subscriptionName,
      }
      axios
        .post(config.serverURL + '/client/register', body)
        .then((response) => {
          const result = response.data
          if (result['status'] === '400') {
            toast.error('Failed registered a new user')
          } else {
            toast.success('successfully registered a new user')
            navigate('/login-client')
          }
        })
        .catch((response) => {
          toast.error('Failed to registered a new user')
        })
    }
  }

  const getPincodeList = () => {
    axios
      .get(config.serverURL + '/address/get_pincode_list')
      .then((response) => {
        const result = []
        result.push('')
        result.push(...response.data)
        setPinCodeList(result)
        // pinCodeList = result
        // if (result['status'] === 'error') {
        //   toast.error('No List Found')
        // } else {
        // }
      })
  }

  const getSubscriptionList = () => {
    axios
      .get(config.serverURL + '/subscription/get_sub_list')
      .then((response) => {
        const result = []
        result.push('')
        result.push(...response.data)
        setSubscriptionList(result)
      })
  }
  useEffect(() => {
    getPincodeList()
    getSubscriptionList()
  }, [])

  const data = [
    {
      label: 'Organization Name',
      func: setName,
      isFullWidth: true,
      pattern:'^[a-zA-Z0-9_][a-zA-Z0-9_ ]*[a-zA-Z0-9_]$',
      //pattern:'^[A-Za-z+_.-]{5,80}$',
      errorMessage:'Name should contain between 5-80 characters',
      required: true,
    },
    {
      label: 'Address1',
      func: setAddress1,
      pattern:'^[A-Za-z]{5,80}$',
      errorMessage:'Address should be supplied',
      required: true,
    },
    {
      label: 'Address2',
      func: setAddress2,
    },
    {
      label: 'PinCode',
      func: setPinCode,
      isDropDown: true,
      listData: pinCodeList,
      pattern:'^(?!\s*$).+',
      errorMessage:'Please select pincode',
      required: true,
    },
    {
      label: 'Select Plan',
      func: setSubscriptionName,
      isDropDown: true,
      listData: subscriptionList,
      isLink: true,
      link: '/plans',
      linkTitle: 'Plan Details',
      
    },
    {
      label: 'Email',
      func: setEmail,
      type:email,
      pattern:'^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$',
      errorMessage:'Please enter valid email address',
      required: true,
    },
    {
      label: 'Contact Number',
      func: setMobile,
    },
    {
      label: 'Password',
      func: setPassword,
      type: 'password',
      pattern:'^[A-Za-z0-9]{3,10}$',
      errorMessage:'Password should contain 3-10 characters',
      required: true,
    },
    {
      label: 'Confirm Password',
      func: setConfirmPassword,
      type: 'password',
      errorMessage:'Password not matched',
      required: true,
    },
  ]

  const submitButton = {
    title: 'Register',
    color: 'btn-success',
    func: register,
  }

  return (
    <div className='bgimage'>
    <RegisterForm
      data={data}
      submitButton={submitButton}
      title='Register'
      noSidebar='true'
    />
    </div>
  )
}

export default RegisterClient
