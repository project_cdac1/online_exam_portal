import HomeContent from '../../components/homeContent'
import Navbar from '../../components/navbar'
import Footer from '../../components/footer'
const Home = () => {
  const dummydata = {
    jwt: false,
  }
  sessionStorage.setItem('userInfo', JSON.stringify(dummydata))
  return (
    <div className='bgimage'>
      <div>
        <Navbar />
      </div>
      <div className='pt-4'>
        <HomeContent />
      </div>
      <Footer/>
    </div>
  )
}

export default Home
