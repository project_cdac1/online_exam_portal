import PlansBox from '../../components/plansbox'
import Navbar from '../../components/navbar'
import Footer from '../../components/footer'
const Plans = () => {
  return (
    <div className='bgimage'>
      <div>
        <Navbar />
      </div>
      <div className='p-5 mt-2'>
        <PlansBox />
      </div>
      <Footer/>
    </div>
  )
}

export default Plans
