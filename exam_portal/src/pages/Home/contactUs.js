import ContactUsBox from '../../components/contactUsBox'
import Footer from '../../components/footer'
import Navbar from '../../components/navbar'

const ContactUs = () => {
  return (
    <div style={{ overflow: 'hidden' }} className='bgimage'>
      <div>
        <Navbar />
      </div>
      <div className='pt-5 mt-5'>
        <ContactUsBox />
      </div>
      <Footer/>
    </div>
  )
}

export default ContactUs
