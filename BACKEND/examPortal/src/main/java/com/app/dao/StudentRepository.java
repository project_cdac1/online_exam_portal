package com.app.dao;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.dto.ExamListDTO;
import com.app.dto.StudentDTO;
import com.app.dto.StudentListDTO;import com.app.pojos.Address;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.GenderEnum;
import com.app.pojos.RoleEnum;
import com.app.pojos.Student;


public interface StudentRepository extends JpaRepository<Student, Long>{
	@Query("SELECT s FROM Student s JOIN FETCH s.client c WHERE s.email =?1 AND s.password =?2")
	Optional<Student> findByEmailAndPassword(String email,String password);
	
	@Query("SELECT s.password FROM Student s WHERE s.email=?1")
	Optional<String> findPasswordByEmail(String email);
	
	@Query("SELECT s.id FROM Student s WHERE s.email=?1")
	Optional<Long> findIdByEmail(String email);
	
	@Query("SELECT new com.app.dto.StudentListDTO(s.id, s.rollNo, s.name, s.email,s.dob, s.gender, s.accStatus) FROM Student s WHERE s.client.id=?1 ORDER BY s.rollNo")
	Optional<List<StudentListDTO>> findListByClientId(Long cId);
	
	@Query("SELECT s FROM Student s JOIN FETCH s.client c WHERE c.id=?1")
	Optional<List<Student>> findByClientId(Long cId);
	
	@Query("SELECT s FROM Student s JOIN FETCH s.client c WHERE s.id=?1")
	Optional<Student> findById(Long id);
	
	Student findByEmail(String email);
	
	@Query("SELECT new com.app.dto.StudentDTO(e.id, e.name, e.email, e.mobile, e.role, e.accStatus,e.dateStamp, e.addressLine1, e.addressLine2, e.address, e.rollNo, e.client.id, e.dob, e.gender) FROM Student e INNER JOIN e.client c WHERE e.email =?1 ")
	StudentDTO findDTOByEmail(String email);
	//@Query("SELECT new com.app.dto.ExamListDTO() FROM Student s INNER JOIN Client c on s.client.id=c.id INNER JOIN Result r on s.id=r.student.id where ")
	 @Query("select new com.app.dto.ExamListDTO(e.id, e.examName, ea.id, e.scheduledDate, e.scheduledTime, e.examStatus, e.examDuration) from Exam e inner join ExamAdmin ea on e.examAdmin.id=ea.id inner join Client c on ea.client.id=c.id where e.id not in( select r.exam.id from Result r where r.student.id=?1 and r.exam.id in (select e.id from Exam e inner join ExamAdmin ea on e.examAdmin.id=ea.id inner join Client c on ea.client.id=c.id))")
	Optional<List<ExamListDTO>> findExamListByStudentId(Long id);
}
