package com.app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.dto.QuestionListDTO;
import com.app.pojos.Exam;
import com.app.pojos.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {
//	@Query("SELECT q FROM Question q WHERE q.exam=?1")
	List<Question> findByExam(Exam exam);

	@Query("SELECT q.exam.id FROM Question q WHERE q.id=?1")
	Optional<Long> findExamIdByQuestionId(long qId);
	
//	List<Exam> findByExam
}
