package com.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Exam;
import com.app.pojos.GradingScheme;

public interface GradingSchemeRepository extends JpaRepository<GradingScheme, Long> {
	Optional<GradingScheme> findByExam(Exam exam);
}
