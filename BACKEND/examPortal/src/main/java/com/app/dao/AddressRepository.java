package com.app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.pojos.Address;

public interface AddressRepository extends JpaRepository<Address, String> {
	Optional<Address> findByPinCode(String pinCode);
	
	@Query("SELECT pinCode FROM Address")
	Optional<List<String>> getAllPincode();
	
	
}
