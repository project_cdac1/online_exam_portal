package com.app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.dto.SubscriptionDTO;
import com.app.pojos.Address;
import com.app.pojos.Subscription;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
	@Query("SELECT planName FROM Subscription")
	Optional<List<String>> getAllSubscription();
	
	Optional<Subscription> findByPlanName(String subscriptionName);
	
	@Query("SELECT new com.app.dto.SubscriptionDTO(s.id,s.planName,s.noOfExams,s.price) FROM Subscription s")
	Optional<List<SubscriptionDTO>> getAllSubscriptionDetails();
}
