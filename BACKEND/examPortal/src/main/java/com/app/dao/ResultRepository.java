package com.app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.dto.ResultDataDTO;
import com.app.dto.StudentFinalResultDTO;
import com.app.pojos.Result;

public interface ResultRepository extends JpaRepository<Result, Long> {
	@Query("SELECT new com.app.dto.ResultDataDTO(q.id, sr.response, q.answer) FROM StudentResponse sr INNER JOIN Question q ON sr.question.id=q.id INNER JOIN Exam e on e.id=q.exam.id WHERE sr.student.id=?1 AND e.id=?2")
	Optional<List<ResultDataDTO>> getResultData(long sId, long exId);
	@Query("SELECT new com.app.dto.StudentFinalResultDTO( r.exam.examName, r.studentMarks, r.totalMarks, r.grade) FROM Result r INNER JOIN Exam e on e.id=r.exam.id WHERE r.student.id=?1")
	Optional<List<StudentFinalResultDTO>> getStudentResult(long sId);
}
