package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.StudentResponse;

public interface StudentResponseRepository extends JpaRepository<StudentResponse, Long> {

}
