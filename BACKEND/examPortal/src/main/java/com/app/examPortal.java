package com.app;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class examPortal {

	public static void main(String[] args) {
		SpringApplication.run(examPortal.class, args);
	}
	@Bean
	public ModelMapper myMapper()
	{
		ModelMapper mapper=new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return mapper;
	}
	//configure password encode bean required for authentication manager
//		@Bean
//		public PasswordEncoder encoder()
//		{
//			return new BCryptPasswordEncoder();
//		}
}
