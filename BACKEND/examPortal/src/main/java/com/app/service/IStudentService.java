package com.app.service;

import java.util.List;

import com.app.dto.ExamListDTO;
import com.app.dto.ExamScheduleDTO;
import com.app.dto.StudentDTO;
import com.app.dto.StudentListDTO;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.Student;

public interface IStudentService {
	Student registerStudent(Student transientStudent, String pinCode,  long clientId);
	StudentDTO updateStudent(Student transientStudent, String pinCode);
	StudentDTO authenticateStudent(String email, String password);
	//String updateStudent(long id);
	//Student findById();
	//String updateStudent(StudentDTO newstudentDto);
	Student editPassword(long id,String oldPassword,String newPassword);
	List<ExamScheduleDTO> getExamSchedule(long cId);
	String activateDeactivateStudent(Long id);
	String getPassword(String email);
	List<StudentListDTO> getStudentList(Long cId);
	Long getStudentIdByEmail(String email);
	List<ExamListDTO> getExamListByStudent(Long sId);
}
