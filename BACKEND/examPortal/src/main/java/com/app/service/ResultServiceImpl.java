package com.app.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ExamRepository;
import com.app.dao.GradingSchemeRepository;
import com.app.dao.QuestionRepository;
import com.app.dao.ResultRepository;
import com.app.dao.StudentRepository;
import com.app.dto.QuestionListDTO;
import com.app.dto.ResultDataDTO;
import com.app.dto.StudentFinalResultDTO;
import com.app.exceptionhandler.custom_exception.ResourceNotFoundException;
import com.app.exceptionhandler.custom_exception.UserNotFoundException;
import com.app.pojos.Exam;
import com.app.pojos.GradingScheme;
import com.app.pojos.Question;
import com.app.pojos.Result;

@Service
@Transactional
public class ResultServiceImpl implements IResultService {
	@Autowired
	private QuestionRepository questionRepo;
	
	@Autowired
	private ResultRepository resultRepo;
	
	@Autowired
	private StudentRepository studentRepo;
	
	@Autowired
	private ExamRepository examRepo;
	
	@Autowired
	private GradingSchemeRepository gradingSchemeRepo;
	
	@Autowired
	private ModelMapper mapper;
	
	@Override
	public List<ResultDataDTO> getResultData(long sId, long exId) {
		return resultRepo.getResultData(sId, exId).orElseThrow(() -> new ResourceNotFoundException("Invalid Student Id or Exam Id"));
	}
	
	@Override
	public String computeAndSaveResult(long sId, long exId) {
		List<ResultDataDTO> resultData = resultRepo.getResultData(sId, exId).orElseThrow(() -> new ResourceNotFoundException("Invalid Student Id or Exam Id"));
		Exam exam = examRepo.findById(exId).orElseThrow();
		GradingScheme gradingScheme = gradingSchemeRepo.findByExam(exam).orElseThrow(() -> new ResourceNotFoundException("Invalid Exam"));
		System.out.println(gradingScheme);
		String grade = null;
		int studentMarks = 0;
		int totalMarks = exam.getMarksPerQues() * exam.getNoOfQues();
		System.out.println(resultData);
		for(ResultDataDTO q : resultData) {
			System.out.println(q);
			if(q.getResponse().equals(q.getAnswer())) {
				studentMarks += exam.getMarksPerQues();
			} else if(!q.getResponse().equals("")){
				studentMarks -= exam.getNegMarksPerQues();
			}
		}
		
		double studentPercentage = ((double)studentMarks/totalMarks)*100;
		System.out.println(studentPercentage);
		if(studentPercentage >= gradingScheme.getGradeA())
			grade = "A";
		else if(studentPercentage >= gradingScheme.getGradeB())
			grade = "B";
		else if(studentPercentage < gradingScheme.getGradeF())
			grade = "F";
		else 
			grade = "C";
		
		resultRepo.save(new Result(studentRepo.findById(sId).orElseThrow(() -> new UserNotFoundException("Invalid Student Id")), exam, studentMarks, totalMarks, grade));
		return "Result computed and submitted Successfully";
	}
	
	@Override
	public List<StudentFinalResultDTO> getFinalResult(long sId) {
		return resultRepo.getStudentResult(sId).orElseThrow(() -> new ResourceNotFoundException("Invalid Student Id "));
	}
}




