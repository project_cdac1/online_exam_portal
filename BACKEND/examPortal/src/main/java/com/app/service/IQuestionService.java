package com.app.service;

import java.util.List;
import java.util.Optional;

import com.app.dto.QuestionListDTO;
import com.app.pojos.Exam;
import com.app.pojos.Question;

public interface IQuestionService {
	Question addQuestion(Question newQuestion, Long examId);
	
	String addQuestionList(List<QuestionListDTO> qList, Long examId);

	Question updateQuestion(Question detachedQuestion);

	List<QuestionListDTO> getQuestionList(long examId);
	
	Long findExamIdByQuestionId(long qId);
}
