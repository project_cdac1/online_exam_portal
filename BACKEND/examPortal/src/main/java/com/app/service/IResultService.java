package com.app.service;

import java.util.List;

import com.app.dto.QuestionListDTO;
import com.app.dto.ResultDataDTO;
import com.app.dto.StudentFinalResultDTO;
import com.app.pojos.Exam;
import com.app.pojos.Result;

public interface IResultService {
	List<ResultDataDTO> getResultData(long sId, long exId);
	String computeAndSaveResult(long sId, long exId);
	List<StudentFinalResultDTO> getFinalResult(long sId);
}
