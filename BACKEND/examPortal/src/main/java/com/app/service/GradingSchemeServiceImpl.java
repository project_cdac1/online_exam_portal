package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ExamRepository;
import com.app.dao.GradingSchemeRepository;
import com.app.exceptionhandler.custom_exception.UserNotFoundException;
import com.app.pojos.Exam;
import com.app.pojos.GradingScheme;

@Service
@Transactional
public class GradingSchemeServiceImpl implements IGradingService {

	@Autowired
	GradingSchemeRepository gsRepo;
	@Autowired
	ExamRepository examRepo;
	@Override
	public GradingScheme addGradingScheme(GradingScheme transientGradingScheme, Long examId) {
		Exam exam = examRepo.findById(examId).orElseThrow(() -> new UserNotFoundException("Invalid Exam Admin Id"));
		transientGradingScheme.setExam(exam);
		return gsRepo.save(transientGradingScheme);
	}

	@Override
	public GradingScheme getGradingScheme(Long eId) {
		Exam exam = examRepo.findById(eId).orElseThrow(() -> new UserNotFoundException("Invalid Exam Admin Id"));
		return gsRepo.findByExam(exam).orElseThrow();
	}
}
