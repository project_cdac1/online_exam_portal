package com.app.service;

import java.util.List;

import com.app.dto.ExamAdminDTO;
import com.app.dto.ExamAdminListDTO;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.Student;

public interface IExamAdminService {
	ExamAdmin registerExamAdmin(ExamAdmin transientExamAdmin, String pinCode,  long clientId);
	ExamAdminDTO authenticateExamAdmin(String email, String password);
	
	ExamAdmin editPassword(long id, String oldPassword, String newPassword);
	ExamAdminDTO updateExamAdmin(ExamAdmin transientExamAdmin, String pinCode);
	String getPassword(String email);
	List<ExamAdminListDTO> getExamAdminList(Long cId);
	String activateDeactivateExamAdmin(long id);
	Long getExamAdminIdByEmail(String email);
}
