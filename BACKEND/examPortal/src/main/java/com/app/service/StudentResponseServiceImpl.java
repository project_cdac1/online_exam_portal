package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.AddressRepository;
import com.app.dao.ClientRepository;
import com.app.dao.ExamAdminRepository;
import com.app.dao.QuestionRepository;
import com.app.dao.StudentRepository;
import com.app.dao.StudentResponseRepository;
import com.app.dao.SubscriptionRepository;
import com.app.dto.LoginRequestDTO;
import com.app.dto.StudentDTO;
import com.app.dto.StudentResponseDTO;
import com.app.exceptionhandler.custom_exception.ResourceNotFoundException;
import com.app.exceptionhandler.custom_exception.UserNotFoundException;
import com.app.pojos.Address;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.Question;
import com.app.pojos.Student;
import com.app.pojos.StudentResponse;
import com.app.pojos.BaseUser;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

@Service
@Transactional
public class StudentResponseServiceImpl implements IStudentResponseService {

	@Autowired
	private StudentRepository studentRepo;
	@Autowired
	private QuestionRepository questionRepo;
	@Autowired
	private ClientRepository clientRepo;
	@Autowired
	private AddressRepository addressRepo;
	@Autowired
	private StudentResponseRepository studentResponseRepo;
	@Autowired
	private ModelMapper mapper;
	
//	@Override
//	public String submitResponse(List<StudentResponseDTO> responseList) {
//		List<StudentResponse> response = new ArrayList<StudentResponse>();
//		for(StudentResponseDTO s : responseList) {
//			Student student = studentRepo.findById(s.getStudentId()).orElseThrow(() -> new UserNotFoundException("Invalid Student Id"));
//			Question question = questionRepo.findById(s.getQuestionId()).orElseThrow(() -> new ResourceNotFoundException("Invalid Question Id"));
//			response.add(new StudentResponse(student, question, s.getResponse()));
//		}
//		studentResponseRepo.saveAll(response);
//		return "Response Submited Successfully";
//	}

	@Override
	public String submitResponse(List<StudentResponseDTO> responseList) {
		List<StudentResponse> response = new ArrayList<StudentResponse>();
		for(StudentResponseDTO s : responseList) {
			Student student = studentRepo.findById(s.getStudentId()).orElseThrow(() -> new UserNotFoundException("Invalid Student Id"));
			Question question = questionRepo.findById(s.getQuestionId()).orElseThrow(() -> new ResourceNotFoundException("Invalid Question Id"));
			response.add(new StudentResponse(student, question, s.getResponse()));
		}
		studentResponseRepo.saveAll(response);
		return "Response Submited Successfully";
	}
}
