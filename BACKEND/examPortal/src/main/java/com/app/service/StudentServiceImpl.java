package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.AddressRepository;
import com.app.dao.ClientRepository;
import com.app.dao.ExamAdminRepository;
import com.app.dao.ExamRepository;
import com.app.dao.StudentRepository;
import com.app.dao.SubscriptionRepository;
import com.app.dto.ExamListDTO;
import com.app.dto.ExamScheduleDTO;
import com.app.dto.LoginRequestDTO;
import com.app.dto.StudentDTO;
import com.app.dto.StudentListDTO;
import com.app.exceptionhandler.custom_exception.IncorrectPasswordException;
import com.app.exceptionhandler.custom_exception.ResourceNotFoundException;
import com.app.exceptionhandler.custom_exception.UserNotFoundException;
import com.app.pojos.Address;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.Student;
import com.app.pojos.BaseUser;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

@Service
@Transactional
public class StudentServiceImpl implements IStudentService {

	@Autowired
	private ClientRepository clientRepo;
	@Autowired
	private AddressRepository addressRepo;
	@Autowired
	private StudentRepository studentRepo;
	@Autowired
	private ExamRepository examRepo;
	@Autowired
	private ModelMapper mapper;
	@Autowired
	private PasswordEncoder encoder;

	@Override
	public Student registerStudent(Student transientStudent, String pinCode, long clientId) {
		transientStudent.setClient(
				clientRepo.findById(clientId).orElseThrow(() -> new UserNotFoundException("Invalid Client Id")));
		transientStudent.setAddress(
				addressRepo.findById(pinCode).orElseThrow(() -> new ResourceNotFoundException("Invalid Pincode")));
		return studentRepo.save(transientStudent);
	}

	@Override
	public StudentDTO authenticateStudent(String email, String password) {
		return mapper.map(studentRepo.findByEmailAndPassword(email, password)
				.orElseThrow(() -> new UserNotFoundException("Invalid email and password")), StudentDTO.class);
	}

	@Override
	public Student editPassword(long id, String oldPassword, String newPassword) {
		Student student = studentRepo.findById(id).orElseThrow(() -> new UserNotFoundException("Invalid Student Id"));
		//student.getEmail();
		System.out.println(oldPassword);
		
//		if (student.getPassword().equals(oldPassword))
			student.setPassword(encoder.encode(newPassword));
//			}
//		else
//			throw new IncorrectPasswordException("Old password is not correct");

		return studentRepo.save(student);
	}

	@Override
	public List<ExamScheduleDTO> getExamSchedule(long cId) {
		return examRepo.getExamSchedule(cId).orElseThrow(() -> new ResourceNotFoundException("Invalid Client Id"));
	}

//	@Override
//	public String updateStudent(StudentDTO newstudentDto) {
//		Student student = studentRepo.findById(newstudentDto.getId()).orElseThrow();
//		
//		return studentRepo.updateStudentDetails(newstudentDto.getId(),newstudentDto.getAddressLine1(),newstudentDto.getAddressLine2(),newstudentDto.getDob(),newstudentDto.getMobile());
//	}

	@Override
	public StudentDTO updateStudent(Student transientStudent, String pinCode) {
		Student student = studentRepo.findById(transientStudent.getId()).orElseThrow(() -> new UserNotFoundException("Invalid Student Id"));
		transientStudent.setAccStatus(student.isAccStatus());
		transientStudent.setClient(student.getClient());
		transientStudent.setPassword(student.getPassword());
		transientStudent.setDateStamp(student.getDateStamp());
		transientStudent.setRole(student.getRole());
		transientStudent.setAddress(addressRepo.findById(pinCode).orElseThrow());
		StudentDTO studentDTO = mapper.map(transientStudent, StudentDTO.class);
		studentRepo.save(transientStudent);
	   return studentDTO;
	}

	@Override
	public String activateDeactivateStudent(Long id) {
		Student student = studentRepo.findById(id).orElseThrow(() -> new UserNotFoundException("Invalid Student Id"));
		if (student.isAccStatus()) {
			student.setAccStatus(false);
			return "Student Account Deactivated SuccessFully";
		} else {
			student.setAccStatus(true);
			return "Student Account Activated SuccessFully";
		}
	}

	@Override
	public String getPassword(String email) {
		return studentRepo.findPasswordByEmail(email).orElseThrow(() -> new ResourceNotFoundException("Invalid email"));
	}
	
	@Override
	public List<StudentListDTO> getStudentList(Long cId) {
		return studentRepo.findListByClientId(cId).orElseThrow(() -> new ResourceNotFoundException("Student List is Empty"));
	}
	
	@Override
	public Long getStudentIdByEmail(String email) {
		return studentRepo.findIdByEmail(email).orElseThrow(() -> new UserNotFoundException("Invalid Email"));
	}

	@Override
	public List<ExamListDTO> getExamListByStudent(Long sId) {
		
		return studentRepo.findExamListByStudentId(sId).orElseThrow();
	}
}
