package com.app.service;

import java.util.List;

import com.app.dto.ExamListDTO;
import com.app.pojos.Exam;

public interface IExamService {
Exam createExam(Exam transientExam, Long ExamAdminId);

	String activateDeactivateExam(Long id);

	List<ExamListDTO> getAllExamByExamAdminId(Long ExamAdminId);
}
