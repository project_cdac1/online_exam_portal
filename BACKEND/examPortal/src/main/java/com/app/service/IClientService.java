package com.app.service;

import java.util.List;

import com.app.dto.ExamListDTO;
import com.app.dto.client.ClientDTO;
import com.app.pojos.Client;
import com.app.pojos.Student;
import com.app.pojos.BaseUser;
import com.app.pojos.Exam;

public interface IClientService {
	Client registerClient(Client transientClient, String pinCode,  String subscriptionName);
	ClientDTO authenticateClient(String email, String password);
	Client editPassword(long id, String oldPassword, String newPassword);
	ClientDTO updateClient(Client transientClient, String pinCode);
	List<ExamListDTO> getAllExamByClient(Long clientId);
	String activateDeactivateClient(long id);
	String getPassword(String email);
	Long getClientIdByEmail(String email);
}
