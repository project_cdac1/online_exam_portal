package com.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ExamRepository;
import com.app.dao.QuestionRepository;
import com.app.pojos.Exam;
import com.app.pojos.Question;
import com.app.dto.QuestionListDTO;
import com.app.exceptionhandler.custom_exception.ResourceNotFoundException;
import com.app.exceptionhandler.custom_exception.UserNotFoundException;
import com.app.pojos.Exam;
import com.app.pojos.Question;

@Service
@Transactional
public class QuestionServiceImpl implements IQuestionService {
	@Autowired
	ExamRepository examRepo;
	
	@Autowired
	QuestionRepository questionRepo;

	@Autowired
	private ModelMapper mapper;
	
	@Override
	public Question addQuestion(Question transientQuestion, Long examId) {
		Exam exam=examRepo.findById(examId).orElseThrow(() -> new UserNotFoundException("Invalid Exam Admin Id"));
		transientQuestion.setExam(exam);
		return questionRepo.save(transientQuestion);
	}
	@Override
	public Question updateQuestion(Question detachedQuestion) {
		Question question=questionRepo.findById(detachedQuestion.getId()).orElseThrow(() -> new ResourceNotFoundException("Invalid Question Id"));
		detachedQuestion.setExam(question.getExam());
		detachedQuestion.setDateStamp(question.getDateStamp());
		
		return questionRepo.save(detachedQuestion);
	}

	
	@Override
	public List<QuestionListDTO> getQuestionList(long examId) {
		List<QuestionListDTO> questionListDTO = new ArrayList<>();
		for(Question q : questionRepo.findByExam(examRepo.findById(examId).orElseThrow(() -> new ResourceNotFoundException("Invalid Exam Id"))))
				questionListDTO.add(mapper.map(q, QuestionListDTO.class));
		return questionListDTO;
	}
	
	@Override
	public Long findExamIdByQuestionId(long qId) {
		return questionRepo.findExamIdByQuestionId(qId).orElseThrow(() -> new ResourceNotFoundException("Invalid Question Id"));
	}
	
	@Override
	public String addQuestionList(List<QuestionListDTO> qList, Long examId) {
//		List<Question> quesList = new ArrayList<>();
		Exam exam = examRepo.findById(examId).orElseThrow(() -> new ResourceNotFoundException("Invalid Exam Id"));
		List<Question> list = questionRepo.findByExam(exam);
		int count = list.size();
		for(QuestionListDTO q : qList) {
			if(exam.getNoOfQues() > count) {
				list.add(new Question(q.getQuesNo(), q.getQuestion(), q.getOptA(), q.getOptB(), q.getOptC(), q.getOptD(), q.getAns(), exam));
			count++;
			} else {
				throw new RuntimeException("Questions are already added");
			}
		}
		questionRepo.saveAll(list);
		return "Question List Added";
	}
}
