package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.AddressRepository;
import com.app.dao.ClientRepository;
import com.app.dao.ExamAdminRepository;
import com.app.dao.SubscriptionRepository;
import com.app.exceptionhandler.custom_exception.ResourceNotFoundException;
import com.app.pojos.Address;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.BaseUser;


@Service
@Transactional
public class AddressServiceImpl implements IAddressService {

	@Autowired
	private AddressRepository addressRepo;
	
	@Override
	public List<String> getAllPincodes() {
		return addressRepo.getAllPincode().orElseThrow(() -> new ResourceNotFoundException("Pincode List is Empty"));
	}
}
