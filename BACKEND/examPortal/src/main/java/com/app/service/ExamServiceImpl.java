package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ClientRepository;
import com.app.dao.ExamAdminRepository;
import com.app.dao.ExamRepository;
import com.app.dto.ExamListDTO;
import com.app.exceptionhandler.custom_exception.SubscriptionExpiredException;
import com.app.exceptionhandler.custom_exception.UserNotFoundException;
import com.app.pojos.Client;
import com.app.pojos.Exam;
import com.app.pojos.ExamAdmin;

@Service
@Transactional
public class ExamServiceImpl implements IExamService {
	@Autowired
	ExamRepository examRepo;
	@Autowired
	ExamAdminRepository examAdminRepo;
	@Autowired
	ClientRepository clientRepo;

	@Override
	public Exam createExam(Exam transientExam, Long ExamAdminId) {
		ExamAdmin examAdmin = examAdminRepo.findById(ExamAdminId).orElseThrow(() -> new UserNotFoundException("Invalid Exam Admin Id"));
		Client client = examAdmin.getClient();
		if (client.getSubscription().getNoOfExams() <= client.getExamCount())
			throw new SubscriptionExpiredException("Exam subscription Expired");
		transientExam.setExamAdmin(examAdmin);
		client.setExamCount(client.getExamCount() + 1);
		return examRepo.save(transientExam);
	}

	@Override
	public String activateDeactivateExam(Long id) {
		Exam exam = examRepo.findById(id).orElseThrow(() -> new UserNotFoundException("Invalid Exam Id"));
		if (exam.isExamStatus()) {
			exam.setExamStatus(false);
			return "Exam Stoped";
		} else {
			exam.setExamStatus(true);
			return "Exam Started";
		}
	}
	
	@Override
	public List<ExamListDTO> getAllExamByExamAdminId(Long ExamAdminId) {
	
		return examRepo.findExamByExamAdminId(ExamAdminId).orElseThrow(()-> new UserNotFoundException("Invalid Exam Admin Id"));
	}
}
