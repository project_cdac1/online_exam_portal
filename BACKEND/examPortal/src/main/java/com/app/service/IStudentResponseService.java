package com.app.service;

import java.util.List;

import com.app.dto.StudentDTO;
import com.app.dto.StudentResponseDTO;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.Student;
import com.app.pojos.StudentResponse;

public interface IStudentResponseService {
	String submitResponse(List<StudentResponseDTO> response);
}
