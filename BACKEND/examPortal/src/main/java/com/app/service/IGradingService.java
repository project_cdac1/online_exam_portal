package com.app.service;

import com.app.pojos.GradingScheme;

public interface IGradingService {
	GradingScheme addGradingScheme(GradingScheme transientGradingScheme ,Long examId );
	GradingScheme getGradingScheme(Long eId);
}
