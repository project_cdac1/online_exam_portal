package com.app.service;

import java.util.List;

import com.app.dto.AdminDTO;
import com.app.dto.client.ClientDTO;
import com.app.dto.client.ClientListDTO;
import com.app.pojos.Client;

public interface IAdminService {
	AdminDTO authenticateAdmin(String email, String password);
	List<ClientListDTO> getClientDetails();
}
