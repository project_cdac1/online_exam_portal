package com.app.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.AddressRepository;
import com.app.dao.AdminRepository;
import com.app.dao.ClientRepository;
import com.app.dao.ExamAdminRepository;
import com.app.dao.ExamRepository;
import com.app.dao.StudentRepository;
import com.app.dao.SubscriptionRepository;
import com.app.dto.AdminDTO;
import com.app.dto.ExamAdminListDTO;
import com.app.dto.ExamListDTO;
import com.app.dto.client.ClientDTO;
import com.app.dto.client.ClientListDTO;
import com.app.exceptionhandler.custom_exception.ResourceNotFoundException;
import com.app.exceptionhandler.custom_exception.UserNotFoundException;
import com.app.pojos.Address;
import com.app.pojos.Client;
import com.app.pojos.Student;
import com.app.pojos.Exam;
import com.app.pojos.Subscription;
import com.app.pojos.BaseUser;

@Service
@Transactional
public class AdminServiceImpl implements IAdminService {

	@Autowired
	private AdminRepository adminRepo;
	@Autowired
	private ClientRepository clientRepo;
	@Autowired
	private StudentRepository studentRepo;
	@Autowired
	private ExamAdminRepository examAdminRepo;
	@Autowired
	private ModelMapper mapper;


	@Override
	public AdminDTO authenticateAdmin(String email, String password) {
		return mapper.map(adminRepo.findByEmailAndPassword(email, password).orElseThrow(() -> new UserNotFoundException("Invalid email or password")), AdminDTO.class);
	}
	
	@Override
	public List<ClientListDTO> getClientDetails() {
		List<ClientListDTO> clientList = clientRepo.getClientDetails().orElseThrow();
		 
		for(ClientListDTO c: clientList)
		{
			List<ExamAdminListDTO> examAdminList=examAdminRepo.findByClientId(c.getId()).orElseThrow();
			 c.setExamAdminCount(examAdminList.size());
			 List<Student> studentList=studentRepo.findByClientId(c.getId()).orElseThrow();
			 c.setStudentCount(studentList.size());
			 
		}
		return clientList;
	}
}
