package com.app.service;

import java.util.List;

import com.app.dto.SubscriptionDTO;
import com.app.pojos.ExamAdmin;
import com.app.pojos.Student;
import com.app.pojos.Subscription;

public interface ISubscriptionService {
	List<String> getSubscriptionList();
	Subscription addSubscription(Subscription transientSubscription);
	Subscription editSubscription(long id,Subscription transientSubscription);
	String deleteSubscription(long id);
	List<SubscriptionDTO> getSubscriptionDetailsList();
	Subscription getSubscriptionDetails(long id);
}
