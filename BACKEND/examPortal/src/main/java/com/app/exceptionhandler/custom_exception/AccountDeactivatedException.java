package com.app.exceptionhandler.custom_exception;

@SuppressWarnings("serial")
public class AccountDeactivatedException extends RuntimeException {
	public AccountDeactivatedException(String mesg) {
		super(mesg);
	}
}
