package com.app.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.ExamAdminRegisterDto;
import com.app.dto.client.ClientRegisterDto;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.RoleEnum;
import com.app.service.IAddressService;
import com.app.service.IClientService;
import com.app.service.IExamAdminService;

@RestController
@RequestMapping("/address")
//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
@Validated
public class AddressController {
	@Autowired
	private IAddressService addressService;
	
	@GetMapping("/get_pincode_list")
	public ResponseEntity<?> getAllPincodes() {
		return ResponseEntity.status(HttpStatus.OK).body(addressService.getAllPincodes());
	}
}
