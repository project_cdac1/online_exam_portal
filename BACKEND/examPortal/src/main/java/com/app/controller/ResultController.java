package com.app.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.ExamAdminRegisterDto;
import com.app.dto.client.ClientRegisterDto;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.RoleEnum;
import com.app.service.IAddressService;
import com.app.service.IClientService;
import com.app.service.IExamAdminService;
import com.app.service.IResultService;

@RestController
@RequestMapping("/result")
//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
@Validated
public class ResultController {
	@Autowired
	private IResultService resultService;

	@PreAuthorize("hasAnyAuthority('EXAM_ADMIN','STUDENT','CLIENT')")
	@GetMapping("/get_result_data")
	public ResponseEntity<?> getResult(@RequestParam long sId, @RequestParam long exId) {
		return ResponseEntity.status(HttpStatus.OK).body(resultService.getResultData(sId, exId));
	}
	@PreAuthorize("hasAnyAuthority('EXAM_ADMIN','STUDENT','CLIENT')")
	@PostMapping("/compute_and_save_result")
	public ResponseEntity<?> computeAndSaveResult(@RequestParam long sId, @RequestParam long exId) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ApiResponse(resultService.computeAndSaveResult(sId, exId)));
	}
}
