package com.app.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.ChangePasswordDTO;
import com.app.dto.ExamAdminRegisterDto;
import com.app.dto.StudentDTO;
import com.app.dto.StudentRegisterDto;
import com.app.dto.StudentResponseDTO;
import com.app.pojos.ExamAdmin;
import com.app.pojos.RoleEnum;
import com.app.pojos.Student;
import com.app.service.IExamAdminService;
import com.app.service.IQuestionService;
import com.app.service.IResultService;
import com.app.service.IStudentResponseService;
import com.app.service.IStudentService;

@RestController
@RequestMapping("/student")
//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
@Validated
public class StudentController {
	@Autowired
	private IStudentService studentService;
	@Autowired
	private IStudentResponseService studentResponseService;
	@Autowired
	private IResultService resultService;
	@Autowired
	private PasswordEncoder encoder;
	@Autowired
	private AuthenticationManager manager;
	@Autowired
	private IQuestionService questionService;
	@PreAuthorize("hasAuthority('CLIENT')")
	@PostMapping("/register")
	public ResponseEntity<?> registerStudent(@RequestBody @Valid StudentRegisterDto newStudentDto) {
		// String name, String email, String mobile, String password, RoleEnum role,
		// String addressLine1, String addressLine2, int rollNo, LocalDate dob, String
		// gender
		studentService.registerStudent(new Student(newStudentDto.getName(), newStudentDto.getEmail(),
				newStudentDto.getMobile(), encoder.encode(newStudentDto.getPassword()), RoleEnum.STUDENT,
				newStudentDto.getAddressLine1(), newStudentDto.getAddressLine2(), newStudentDto.getRollNo(),
				newStudentDto.getDob(), newStudentDto.getGender()), newStudentDto.getPinCode(),
				newStudentDto.getClientId());
		return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("User Registered successfully"));
	}
	@PreAuthorize("hasAuthority('STUDENT')")
	@PutMapping("/edit_password/{id}")
	public ResponseEntity<?> editStudentPassword(@PathVariable long id,
			@RequestBody ChangePasswordDTO changePasswordDto) {
		
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(changePasswordDto.getEmail(),
				changePasswordDto.getOldPassword());
		try {
			// authenticate the credentials
			Authentication authenticatedDetails = manager.authenticate(authToken);
		// long id=newStudentDto.getId();
		// String name, String email, String mobile, String password, RoleEnum role,
		// String addressLine1, String addressLine2, int rollNo, LocalDate dob, String
		// gender
		Student student = studentService.editPassword(id, encoder.encode(changePasswordDto.getOldPassword()),
				changePasswordDto.getNewPassword());
		if (student != null) {
			return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("password changed"));
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("edit password failed"));
		
	} catch (BadCredentialsException e) { // lab work : replace this by a method in global exc handler
		// send back err resp code
		System.out.println("err " + e);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
	}
		//return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("edit password failed"));
	}
	@PreAuthorize("hasAuthority('STUDENT')")
	@GetMapping("/imp_dates/{cId}")
	public ResponseEntity<?> getExamSchedule(@PathVariable long cId) {
		return ResponseEntity.status(HttpStatus.CREATED).body(studentService.getExamSchedule(cId));
	}
	@PreAuthorize("hasAuthority('STUDENT')")
	@PutMapping("/update_student")
	public ResponseEntity<?> updateStudent(@RequestBody @Valid StudentRegisterDto newStudentDto) {
		System.out.println("inside update student "+newStudentDto.getId());
		// String name, String email, String mobile, String password, RoleEnum role,
		// String addressLine1, String addressLine2, int rollNo, LocalDate dob, String
		// gender
		StudentDTO studentDto=studentService.updateStudent(new Student(newStudentDto.getId(), newStudentDto.getName(),
				newStudentDto.getEmail(), newStudentDto.getMobile(), RoleEnum.STUDENT, newStudentDto.getAddressLine1(),
				newStudentDto.getAddressLine2(), newStudentDto.getRollNo(), newStudentDto.getDob(),
				newStudentDto.getGender()), newStudentDto.getPinCode());
		return ResponseEntity.status(HttpStatus.CREATED).body(studentDto);
	}
	@PreAuthorize("hasAuthority('CLIENT')")
	@PutMapping("/toggle_student_acc_status/{id}")
	public ResponseEntity<?> activateDeactivateStudent(@PathVariable long id) {
		// String name, String email, String mobile, String password, RoleEnum role,
		// String addressLine1, String addressLine2, int rollNo, LocalDate dob, String
		// gender
		return ResponseEntity.status(HttpStatus.ACCEPTED)
				.body(new ApiResponse(studentService.activateDeactivateStudent(id)));
	}
	@PreAuthorize("hasAnyAuthority('EXAM_ADMIN','STUDENT','CLIENT')")
	@PostMapping("/submit_response")
	public ResponseEntity<?> submitResponse(@RequestBody List<StudentResponseDTO> responseList) {
		
		StudentResponseDTO response = responseList.get(0);
		studentResponseService.submitResponse(responseList);
		long exId = questionService.findExamIdByQuestionId(response.getQuestionId());
		System.out.println(exId);
		resultService.computeAndSaveResult(response.getStudentId(), exId);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse("Response Submitted Successfully"));
	}
	@PreAuthorize("hasAnyAuthority('EXAM_ADMIN','STUDENT','CLIENT')")
	@GetMapping("/get_student_list/{id}")
	public ResponseEntity<?> getExamAdminList(@PathVariable long id) {
		return ResponseEntity.status(HttpStatus.OK).body(studentService.getStudentList(id));
	}
	@PreAuthorize("hasAnyAuthority('EXAM_ADMIN','STUDENT','CLIENT')")
	@GetMapping("/get_student_result/{sId}")
	public ResponseEntity<?> getStudentResult(@PathVariable long sId) {
		System.out.println("inside impdates "+sId);
		return ResponseEntity.status(HttpStatus.CREATED).body(resultService.getFinalResult(sId));
	}
}
