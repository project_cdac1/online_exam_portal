package com.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.ChangePasswordDTO;
import com.app.dto.LoginRequestDTO;
import com.app.dto.client.ClientRegisterDto;
import com.app.dto.client.ClientUpdateDto;
import com.app.pojos.Client;
import com.app.pojos.RoleEnum;
import com.app.pojos.Student;
import com.app.service.IAdminService;
import com.app.service.IClientService;
import com.app.service.IExamAdminService;

@RestController
@RequestMapping("/client")
//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
@Validated
public class ClientController {
	@Autowired
	private IClientService clientService;
	@Autowired
	private IAdminService adminService;
	@Autowired
	private IExamAdminService examAdminService;
	@Autowired
	private AuthenticationManager manager;
	@Autowired
	private PasswordEncoder encoder;
	@PostMapping("/register")
	public ResponseEntity<?> registerClient(@RequestBody @Valid ClientRegisterDto newClientDto) {
		// String name, String email, String mobile, String password, RoleEnum role
		clientService.registerClient(new Client(newClientDto.getName(), newClientDto.getEmail(),
				newClientDto.getMobile(), encoder.encode(newClientDto.getPassword()), RoleEnum.CLIENT, newClientDto.getAddressLine1(),
				newClientDto.getAddressLine2()), newClientDto.getPinCode(), newClientDto.getSubscriptionName());
		return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("User Registered successfully"));
	}
	@PreAuthorize("hasAuthority('CLIENT')")
	@PutMapping("/edit_password/{id}")
	public ResponseEntity<?> editClientPassword(@PathVariable long id,
			@RequestBody ChangePasswordDTO changePasswordDto) {
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(changePasswordDto.getEmail(),
				changePasswordDto.getOldPassword());
		try {
			// authenticate the credentials
			Authentication authenticatedDetails = manager.authenticate(authToken);
		// long id=newStudentDto.getId();
		// String name, String email, String mobile, String password, RoleEnum role,
		// String addressLine1, String addressLine2, int rollNo, LocalDate dob, String
		// gender
		clientService.editPassword(id, encoder.encode(changePasswordDto.getOldPassword()),
				changePasswordDto.getNewPassword());
		
			return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("password changed"));
	
		
	} catch (BadCredentialsException e) { // lab work : replace this by a method in global exc handler
		// send back err resp code
		System.out.println("err " + e);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
	}
		//return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("edit password failed"));
	}
//		clientService.editPassword(id, changePasswordDto.getOldPassword(),
//				changePasswordDto.getNewPassword());
//		return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("password changed"));
//	}
	
//	@PutMapping("/edit_password/{id}")
//	public ResponseEntity<?> editClientPassword(@PathVariable long id,
//			@RequestBody ChangePasswordDTO changePasswordDto) {
//		
//			Client client = clientService.editPassword(id, changePasswordDto.getOldPassword(),
//					changePasswordDto.getNewPassword());
//			if (client != null) {
//				return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("password changed"));
//			}
//			return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("edit password failed"));
//	}
	@PreAuthorize("hasAuthority('CLIENT')")
	@PostMapping("/update_client")
	public ResponseEntity<?> updtateClient(@RequestBody @Valid ClientUpdateDto newClientDto) {
		// String name, String email, String mobile, String password, RoleEnum role

		return ResponseEntity.status(HttpStatus.CREATED).body(clientService.updateClient(
				new Client(newClientDto.getId(), newClientDto.getName(), newClientDto.getEmail(),
						newClientDto.getMobile(), newClientDto.getAddressLine1(), newClientDto.getAddressLine2()),
				newClientDto.getPinCode()));
	}

	
	@PreAuthorize("hasAuthority('ADMIN')")
	@PutMapping("/toggle_client_acc_status/{id}")
	public ResponseEntity<?> activateDeactivateClient(@PathVariable long id) {
		return ResponseEntity.status(HttpStatus.ACCEPTED)
				.body(new ApiResponse(clientService.activateDeactivateClient(id)));
	}
//	@PreAuthorize("hasAuthority('ADMIN')")
//	@GetMapping("/get_client_details")
//	public ResponseEntity<?> getClientList() {
//		return ResponseEntity.status(HttpStatus.ACCEPTED).body(adminService.getClientDetails());
//	}

//	@GetMapping("/get_exam_admin_list/{id}")
//	public ResponseEntity<?> getExamAdminList(@PathVariable long id) {
//		return ResponseEntity.status(HttpStatus.FOUND).body(examAdminService.getExamAdminList(id));
//	}
}
