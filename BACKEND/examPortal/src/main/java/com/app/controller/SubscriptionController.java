package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.SubscriptionDTO;
import com.app.pojos.Subscription;
import com.app.service.ISubscriptionService;

@RestController
@RequestMapping("/subscription")
//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
public class SubscriptionController {
	@Autowired
	private ISubscriptionService subscriptionService;
	
	
	@GetMapping("/get_sub_list")
	public ResponseEntity<?> getAllSubscriptions() {
		System.out.println("Inside @GetMapping get_sub_list" + subscriptionService.getSubscriptionList());
		return ResponseEntity.status(HttpStatus.OK).body(subscriptionService.getSubscriptionList());
	}
	@PreAuthorize("hasAuthority('ADMIN')")
	@PostMapping("/add_subscription")
	public ResponseEntity<?> addSubscription(@RequestBody SubscriptionDTO newSubscriptionDto) {
		// String name, String email, String mobile, String password, RoleEnum role
		subscriptionService.addSubscription(new Subscription(newSubscriptionDto.getPlanName(),
				newSubscriptionDto.getNoOfExams(), newSubscriptionDto.getPrice()));
		return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("Subscription added successfully"));
	}
	@PreAuthorize("hasAuthority('ADMIN')")
	@PutMapping("/edit_subscription")
	public ResponseEntity<?> editSubscription(@RequestBody SubscriptionDTO newSubscriptionDto) {
		// String name, String email, String mobile, String password, RoleEnum role
		subscriptionService.editSubscription(newSubscriptionDto.getId(), new Subscription(
				newSubscriptionDto.getPlanName(), newSubscriptionDto.getNoOfExams(), newSubscriptionDto.getPrice()));
		return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("Subscription edited successfully"));
	}
	@PreAuthorize("hasAuthority('ADMIN')")
	@DeleteMapping("/delete_subscription/{id}")
	public ResponseEntity<?> deleteSubscription(@PathVariable long id) {
		// String name, String email, String mobile, String password, RoleEnum role
		return ResponseEntity.status(HttpStatus.CREATED).body(subscriptionService.deleteSubscription(id));
	}
	
	@GetMapping("/get_sub_details")
	public ResponseEntity<?> getAllSubscriptionDetails() {
		//System.out.println("Inside @GetMapping get_sub_list" + subscriptionService.getSubscriptionList());
		return ResponseEntity.status(HttpStatus.OK).body(subscriptionService.getSubscriptionDetailsList());
	}
	
	@GetMapping("/get_sub_by_id/{id}")
	public ResponseEntity<?> getSubscriptionDetails(@PathVariable long id) {
		//System.out.println("Inside @GetMapping get_sub_list" + subscriptionService.getSubscriptionList());
		return ResponseEntity.status(HttpStatus.OK).body(subscriptionService.getSubscriptionDetails(id));
	}
}
