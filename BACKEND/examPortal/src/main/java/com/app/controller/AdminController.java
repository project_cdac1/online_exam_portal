package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.ChangePasswordDTO;
import com.app.dto.LoginRequestDTO;
import com.app.dto.client.ClientRegisterDto;
import com.app.pojos.Client;
import com.app.pojos.RoleEnum;
import com.app.pojos.Student;
import com.app.service.IAdminService;
import com.app.service.IClientService;

@RestController
@RequestMapping("/admin")
//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
@Validated
public class AdminController {
	@Autowired
	private IClientService clientService;
	@Autowired
	private IAdminService adminService;

	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping("/get_client_details")
	public ResponseEntity<?> getClientList() {
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(adminService.getClientDetails());
	}

}
