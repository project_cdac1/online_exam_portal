package com.app.controller;

import java.time.LocalDate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.ChangePasswordDTO;
import com.app.dto.ExamAdminRegisterDto;
import com.app.dto.client.ClientRegisterDto;
import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.RoleEnum;
import com.app.pojos.Student;
import com.app.service.IClientService;
import com.app.service.IExamAdminService;

@RestController
@RequestMapping("/exam_admin")
//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
@Validated
public class ExamAdminController {
	@Autowired
	private IExamAdminService examAdminService;
	@Autowired
	private PasswordEncoder encoder;
	@Autowired
	private AuthenticationManager manager;
	
	@PreAuthorize("hasAuthority('CLIENT')")
	@PostMapping("/register")
	public ResponseEntity<?> registerExamAdmin(@RequestBody @Valid ExamAdminRegisterDto newExamAdminDto) {
		examAdminService
				.registerExamAdmin(
						new ExamAdmin(newExamAdminDto.getName(), newExamAdminDto.getEmail(),
								newExamAdminDto.getMobile(), encoder.encode(newExamAdminDto.getPassword()), RoleEnum.EXAM_ADMIN,
								newExamAdminDto.getAddressLine1(), newExamAdminDto.getAddressLine2(),
								newExamAdminDto.getDepartment(), newExamAdminDto.getDob(), newExamAdminDto.getGender()),
						newExamAdminDto.getPinCode(), newExamAdminDto.getClientId());
		return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("User Registered successfully"));
	}
	@PreAuthorize("hasAuthority('EXAM_ADMIN')")
	@PutMapping("/edit_password/{id}")
	public ResponseEntity<?> editExamAdminPassword(@PathVariable long id,
			@RequestBody ChangePasswordDTO changePasswordDto) {
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(changePasswordDto.getEmail(),
				changePasswordDto.getOldPassword());
		try {
			// authenticate the credentials
			Authentication authenticatedDetails = manager.authenticate(authToken);
		// long id=newStudentDto.getId();
		// String name, String email, String mobile, String password, RoleEnum role,
		// String addressLine1, String addressLine2, int rollNo, LocalDate dob, String
		// gender
		examAdminService.editPassword(id, encoder.encode(changePasswordDto.getOldPassword()),
				changePasswordDto.getNewPassword());
		
			return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("password changed"));
	
		
	} catch (BadCredentialsException e) { // lab work : replace this by a method in global exc handler
		// send back err resp code
		System.out.println("err " + e);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
	}
		//return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("edit password failed"));
	}
	@PreAuthorize("hasAuthority('EXAM_ADMIN')")
	@PostMapping("/update_exam_admin")
	public ResponseEntity<?> updateExamAdmin(@RequestBody @Valid ExamAdminRegisterDto examAdminDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(examAdminService.updateExamAdmin(new ExamAdmin(examAdminDto.getId(),
						examAdminDto.getName(), examAdminDto.getEmail(), examAdminDto.getMobile(), RoleEnum.EXAM_ADMIN,
						examAdminDto.getAddressLine1(), examAdminDto.getAddressLine2(), examAdminDto.getDepartment(),
						examAdminDto.getDob(), examAdminDto.getGender()), examAdminDto.getPinCode()));
	}
	@PreAuthorize("hasAuthority('CLIENT')")
	@GetMapping("/get_exam_admin_list/{id}")
	public ResponseEntity<?> getExamAdminList(@PathVariable long id) {
		return ResponseEntity.status(HttpStatus.OK).body(examAdminService.getExamAdminList(id));
	}
	@PreAuthorize("hasAuthority('CLIENT')")
	@PutMapping("/toggle_exam_admin_acc_status/{id}")
	public ResponseEntity<?> activateDeactivateExamAdmin(@PathVariable long id) {
		return ResponseEntity.status(HttpStatus.ACCEPTED)
				.body(new ApiResponse(examAdminService.activateDeactivateExamAdmin(id)));
	}
	
	
}
