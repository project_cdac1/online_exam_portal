package com.app.dto;

import com.app.pojos.RoleEnum;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LoginRequestDTO {
	private String email;
	private String password;
	private String role;
}
