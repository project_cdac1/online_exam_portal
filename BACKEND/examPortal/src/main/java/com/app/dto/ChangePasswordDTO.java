package com.app.dto;

import com.app.pojos.RoleEnum;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChangePasswordDTO {
	private String oldPassword;
	private String newPassword;
	private String email;
}
