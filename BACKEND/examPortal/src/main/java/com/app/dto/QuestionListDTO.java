package com.app.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

import com.app.pojos.Exam;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QuestionListDTO {
//	private long id;
//	private int quesNo;
//	private String question;
//	private String optA;
//	private String optB;
//	private String optC;
//	private String optD;
//	private String ans;
//	private String answer;
	@NotBlank(message = "Exam ID  must be supplied")
	private Long id;
	@NotBlank(message = "Question No  must be supplied")
	private int quesNo; 
	@NotBlank(message = "Question  must be supplied")
	private String question;
	@NotBlank(message = "All options  must be supplied")
	private String optA;
	@NotBlank(message = "All options  must be supplied")
	private String optB;
	@NotBlank(message = "All options  must be supplied")
	private String optC;
	@NotBlank(message = "All options  must be supplied")
	private String optD;
	private String answer;
	private String ans;
	public QuestionListDTO(long id, int quesNo, String question, String optA, String optB, String optC, String optD) {
		super();
		this.id = id;
		this.quesNo = quesNo;
		this.question = question;
		this.optA = optA;
		this.optB = optB;
		this.optC = optC;
		this.optD = optD;
		this.ans = "";
	}
	
	
}
