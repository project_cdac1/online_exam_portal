package com.app.dto;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import com.app.pojos.Exam;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AddGradingSchemeDTO {
	@NotBlank(message = "Exam ID must be supplied")
	private Long examId;
	@NotBlank(message = "All Grades must be supplied")
	private int gradeA;
	@NotBlank(message = "All Grades must be supplied")
	private int gradeB;
	@NotBlank(message = "All Grades must be supplied")
	private int gradeC;
	@NotBlank(message = "All Grades must be supplied")
	private int gradeF;
}
