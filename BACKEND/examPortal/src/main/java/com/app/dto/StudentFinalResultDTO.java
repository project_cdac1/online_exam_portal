package com.app.dto;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.app.pojos.Exam;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudentFinalResultDTO {
	
	//private long studentId;
	private String examName;
	
	private int studentMarks;
	
	
	private int totalMarks;
	

	private String grade;
	
}
