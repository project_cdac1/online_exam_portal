package com.app.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddQuestionDTO {
@NotBlank(message = "Exam name  must be supplied")
private String exam;
@NotBlank(message = "Question No  must be supplied")
private int quesNo; 
@NotBlank(message = "Question  must be supplied")
private String question;
@NotBlank(message = "All options  must be supplied")
private String optA;
@NotBlank(message = "All options  must be supplied")
private String optB;
@NotBlank(message = "All options  must be supplied")
private String optC;
@NotBlank(message = "All options  must be supplied")
private String optD;
@NotBlank(message = "Answer  must be supplied")
private String answer;
@NotBlank(message = "Exam ID  must be supplied")
private Long examId;
}
