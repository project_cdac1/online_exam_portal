package com.app.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.app.pojos.RoleEnum;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RecoverPasswordDTO {
	@NotBlank(message = "Email must be supplied")
	@Email(message = "Invalid Email Format")
	private String email;
	@NotBlank(message = "Old Password must be supplied")
	private String oldPassword;
	@NotBlank(message = "New Password must be supplied")
	private String newPassword;
	private String role;
}
