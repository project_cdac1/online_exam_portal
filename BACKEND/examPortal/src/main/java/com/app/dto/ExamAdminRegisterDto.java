package com.app.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.pojos.Address;
import com.app.pojos.GenderEnum;
import com.app.pojos.RoleEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = "password")
public class ExamAdminRegisterDto {
//	private Long id;
//	private String name;
//	private String email;
//	private String mobile;
//	private String password;
//	private long clientId;
//	private String pinCode;
//	private String addressLine1;
//	private String addressLine2;
//	private String department;
//	private LocalDate dob;
//	private String gender;
	
	private Long id;
	@NotBlank(message = " name  must be supplied")
	private String name;
	@NotBlank(message = "Email must be supplied")
	@Email(message = "Invalid Email Format")
	private String email;
	private String mobile;
	@NotBlank(message = "password must be supplied")
	@JsonProperty(access =Access.WRITE_ONLY)
    private String password;
	@NotBlank(message = "Pincode must be supplied")
	private String pinCode;
	@NotBlank(message = " address  must be supplied")
	private String addressLine1;
	private String addressLine2;
	@NotBlank(message = " department  must be supplied")
	private String department;
	//@NotNull(message = " Client ID  must be supplied")
	private Long clientId;
	@Past(message = "  Date of Birth should not in future...")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dob;
	@NotBlank(message = " Gender  must be supplied")
	private String gender;
}
