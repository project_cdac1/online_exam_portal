package com.app.dto;

import java.time.LocalDateTime;

import javax.persistence.Entity;

import com.app.pojos.Exam;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GradingSchemeDTO {
	private Exam exam;
	private int gradeA;
	private int gradeB;
	private int gradeC;
	private int gradeF;
}
