package com.app.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import com.app.pojos.Address;
import com.app.pojos.Client;
import com.app.pojos.GenderEnum;
import com.app.pojos.RoleEnum;
import com.app.pojos.Subscription;
import com.app.pojos.BaseUser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentDTO {
	private Long id;
	private String name;
	private String email;
	private String mobile;
//	private String password;
	private RoleEnum role;
	private boolean accStatus;
	private LocalDateTime dateStamp;
	private String addressLine1;
	private String addressLine2;
	private Address address;
	private int rollNo;
	private Long clientId;
	private LocalDate dob;
	private GenderEnum gender;
	//e.id, e.name, e.email, e.mobile, e.role, e.accStatus,e.dateStamp, e.addressLine1, e.addressLine2, e.address, e.rollNo, e.client.id, e.dob, e.gender
}
