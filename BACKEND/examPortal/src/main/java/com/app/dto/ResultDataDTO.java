package com.app.dto;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ResultDataDTO {
	@NotBlank(message = " Question ID   must be supplied")
	private long questionId;
	@NotBlank(message = "response  must be supplied")
	private String response;
	@NotBlank(message = " Answer  must be supplied")
	private String answer;
}
