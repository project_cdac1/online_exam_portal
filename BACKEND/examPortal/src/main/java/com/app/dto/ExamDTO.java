package com.app.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.app.pojos.Client;
import com.app.pojos.ExamAdmin;
import com.app.pojos.GenderEnum;
import com.app.pojos.RoleEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ExamDTO {
private String	 ExamName;
private String     NoOfQuestions;
private int    MarksPerQuestion;
private int    NegativeMarksPerQuestion;
private LocalTime     ExamDuration;
private LocalDate    ExamDate;
private boolean ExamStatus;
private ExamAdmin ExamAdmin;

}
