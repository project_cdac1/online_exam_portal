package com.app.dto;

import com.app.pojos.Exam;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class QuestionDTO {

	private int quesNo; 
	private String question;
	private String optA;
	private String optB;
	private String optC;
	private String optD;
	private String answer;
	private Exam exam;
}
