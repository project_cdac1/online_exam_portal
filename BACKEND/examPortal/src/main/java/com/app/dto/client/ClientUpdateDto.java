package com.app.dto.client;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.app.pojos.RoleEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString(exclude = "password")
public class ClientUpdateDto {
	private Long id;
	@NotBlank(message = " name  must be supplied")
	private String name;
	@NotBlank(message = "Email must be supplied")
	@Email(message = "Invalid Email Format")
	private String email;
	private String mobile;
	
	
	
	@NotBlank(message = "Pincode must be supplied")
	private String pinCode;
	@NotBlank(message = " address  must be supplied")
	private String addressLine1;
	private String addressLine2;
}
