package com.app.dto.client;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.app.pojos.Address;
import com.app.pojos.RoleEnum;
import com.app.pojos.Subscription;
import com.app.pojos.BaseUser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/*
 * Client
org_type
subscription_id(fk)(un)
exam_count
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClientDTO {
	private Long id;
	private String name;
	private String email;
	private String mobile;
	//private String password;
	private RoleEnum role;
	private boolean accStatus;
	private LocalDateTime dateStamp;
	private String addressLine1;
	private String addressLine2;
	private Address address;
	private Subscription subscription;
	private int examCount;
	
	
	public ClientDTO(Long id, String name, String email, String mobile, boolean accStatus,
			LocalDateTime dateStamp, String addressLine1, String addressLine2, Address address,
			Subscription subscription, int examCount) {
		super();
		this.id=id;
		this.name = name;
		this.email = email;
		this.mobile = mobile;
		this.accStatus = accStatus;
		this.dateStamp = dateStamp;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.address = address;
		this.subscription = subscription;
		this.examCount = examCount;
	}
	
	
}
