package com.app.dto;

import com.app.dto.client.ClientDTO;
import com.app.pojos.Client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AuthClient {
	private String message;
	private String jwt;
	private ClientDTO client;
	
}
