package com.app.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import com.app.pojos.Address;
import com.app.pojos.Client;
import com.app.pojos.GenderEnum;
import com.app.pojos.RoleEnum;
import com.app.pojos.Subscription;
import com.app.pojos.BaseUser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/*
 * Client
org_type
subscription_id(fk)(un)
exam_count
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ExamAdminDTO {
	private Long id;
	@NotBlank(message = " name  must be supplied")
	private String name;
	@NotBlank(message = "Email must be supplied")
	@Email(message = "Invalid Email Format")
	private String email;
	private String mobile;
//	private String password;
	private RoleEnum role;
	private boolean accStatus;
	private LocalDateTime dateStamp;
	@NotBlank(message = " address  must be supplied")
	private String addressLine1;
	private String addressLine2;
	@NotBlank(message = " address  must be supplied")
	private Address address;
	@NotBlank(message = " department  must be supplied")
	private String department;
	@NotBlank(message = " Client ID  must be supplied")
	private Long clientId;
	@Future(message = "Birth of Date should not in future...")
	private LocalDate dob;
	@NotBlank(message = " Gender  must be supplied")
	private GenderEnum gender;
	//c.id, c.name, c.email, c.mobile, c.accStatus, c.dateStamp, c.addressLine1, c.addressLine2,c.address,c.department, c.client, c.dob, ,c.gender
	
//	e.id, e.name, e.email, e.mobile, e.role, e.accStatus,e.dateStamp, e.addressLine1, e.addressLine2, e.address, e.department, e.client, e.dob, e.gender
	public ExamAdminDTO(String name, String email, String department) {
		super();
		this.name = name;
		this.email = email;
		this.department = department;
	}
	
	
}
