package com.app.dto;

import com.app.pojos.Question;
import com.app.pojos.Student;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudentResponseDTO {
	private long studentId;
	private long questionId;
	private String response;
}
