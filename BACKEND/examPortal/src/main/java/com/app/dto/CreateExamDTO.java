package com.app.dto;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.validation.constraints.NotBlank;

import com.app.pojos.ExamAdmin;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class CreateExamDTO {
@NotBlank(message = "Exam name  must be supplied")	
private String examName;
@NotBlank(message = "No of questions  must be supplied")
private int noOfQues;
@NotBlank(message = "marks Per questions must be supplied")
private int marksPerQues;
@NotBlank(message = "negMarks Per questions must be supplied")
private int negMarksPerQues;
@NotBlank(message = "Scheduled Time must be supplied")
private LocalTime scheduledTime;
@NotBlank(message = "Scheduled Date must be supplied")
private LocalDate scheduledDate;
@NotBlank(message = "Exam Status must be supplied")
private boolean examStatus;
@NotBlank(message = "Exam Admin ID must be supplied")
private Long examAdminID;
@NotBlank(message = "Exam Duration  must be supplied")
private int examDuration;
@NotBlank(message = "result Date must be supplied")
private LocalDate resultDate;
}
