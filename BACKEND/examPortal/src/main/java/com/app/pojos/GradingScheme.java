package com.app.pojos;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/*
 * 
Grading_scheme

ex_id(fk)(un)
grade_A
grade_B
grade_C
grade_F
date_timestamp

 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "dateStamp")
public class GradingScheme extends BaseEntity {
	@OneToOne(fetch = FetchType.LAZY) 
	@JoinColumn(name="ex_id",nullable = false)
	@MapsId 
	private Exam exam;
	
	// public GradingScheme(int gradeA, int gradeB, int gradeC, int gradeF) {
	// 	super();
	// 	this.gradeA = gradeA;
	// 	this.gradeB = gradeB;
	// 	this.gradeC = gradeC;
	// 	this.gradeF = gradeF;
	// }

	@Column(nullable = false)
	private int gradeA;
	
	@Column(nullable = false)
	private int gradeB;
	
	@Column(nullable = false)
	private int gradeC;
	
	@Column(nullable = false)
	private int gradeF;
	
	@Column(nullable = false)
	@CreationTimestamp
	private LocalDateTime dateStamp;
	
	public GradingScheme(int gradeA, int gradeB, int gradeC, int gradeF) {
		super();
		this.gradeA = gradeA;
		this.gradeB = gradeB;
		this.gradeC = gradeC;
		this.gradeF = gradeF;
	}
}
